import { Main } from "../main.js";

export class Sprite {
	constructor(cacheID, x, y, width, height) {
		this.cacheID = cacheID;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		if(Main.offscreenCanvasSupported) {
			this.tmpCanvas = new OffscreenCanvas(width, height);
		} else {
			this.tmpCanvas = document.createElement("canvas");
			this.tmpCanvas.width = width;
			this.tmpCanvas.height = height;
		}
	}

	draw(context, drawX, drawY, scale = 1, rotation = null) {
    	context.save();
  		context.translate(drawX, drawY); // pivot point
    	context.rotate(rotation);

   		context.drawImage(SpriteManager.imgCache[this.cacheID], this.x, this.y, this.width, this.height,
   				-this.width / 2 * scale,
   				-this.height / 2 * scale,
   				this.width * scale,
   				this.height * scale);

    	context.restore();
	}

	drawTinted(context, drawX, drawY, color, scale = 1, rotation = 0) {
		let tmpContext = this.tmpCanvas.getContext("2d");
		tmpContext.globalCompositeOperation = "copy";
		tmpContext.drawImage(SpriteManager.imgCache[this.cacheID], this.x, this.y, this.width, this.height,
			0, 0, this.width, this.height);
		tmpContext.globalCompositeOperation = "source-in";
		tmpContext.fillStyle = color;
		tmpContext.fillRect(0, 0, this.width, this.height);

		context.save();
  		context.translate(drawX, drawY); // pivot point
    	context.rotate(rotation);

    	context.drawImage(this.tmpCanvas, 0, 0, this.width, this.height,
    		-this.width / 2 * scale,
    		-this.height / 2 * scale,
    		this.width * scale,
    		this.height * scale);

    	context.restore();
	}
}

class SpriteSheet {
	constructor(name, width, height) {
		this.name = name;
		this.width = width;
		this.height = height;
	}
}

export const SpriteManager = {
	init(onLoadCallback) {
		this.onLoadCallback = onLoadCallback;
		this.imgCache = [];

		this.spriteSheets = [
			new SpriteSheet("spritesheet", 800, 600)
		];
		
		Promise.all(this.spriteSheets.map((ss, index) => this.loadSpriteSheet(ss, index))).then(images => {
			this.onLoadCallback()
		});

		this.sprites = [
			new Sprite(0, 50, 0, 12, 12),
			new Sprite(0, 0, 0, 48, 48)
		];
		for(let i = 0; i < 50; i++) {
			const spriteX = i % 20;
			const spriteY = Math.floor(i / 20);
			this.sprites.push(new Sprite(0, 1 + spriteX * 20, 51 + spriteY * 20, 18, 18));
		}
	},

	loadSpriteSheet(spriteSheet, index) {
		return new Promise(resolve => {
			// setTimeout(() => {
			let img = new Image();

			// sleep(5000);

			img.onload = function() {
				this.imgCache[index] = img;
				resolve(img);
			}.bind(this);
			img.src = "assets/sprites/" + spriteSheet.name + ".png";
			img.width = spriteSheet.width;
			img.height = spriteSheet.height;
			// }, 5000);
		});
	}
}

export const SpriteIDs = {
	bulletNoShadow: 0,
	bulletShadow: 1,
	starSheet: 2,
	next: 52
}
