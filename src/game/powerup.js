import { Main } from "../main.js";
import { ColorData } from "./data.js";
import { EntityManager } from "../entities/entity.js";
import { TextPopup } from "../entities/text-popup.js";
import { Lang } from "../lang/lang.js";

export class Powerup {
	constructor(id, color) {
		this.id = id;
		this.color = color;
	}

	apply(player) {
	}

	update(player) {
		if(player.powerupTime > 0)
			player.powerupTime--;
	}

	lastUpdate(player) {

	}

	isActive(player) {
		return player.powerupTime > 0;
	}

	drawIcon(context, x, y) {

	}
}

export class SlowmoPowerup extends Powerup {
	constructor(id) {
		super(id, ColorData.asteroids[3]);
	}

	apply(player) {
		Main.slowmoTimer = 200;
		EntityManager.add(TextPopup, new TextPopup(player.pos.copy(), Lang.get("gameplay.slowmo_popup"), ColorData.asteroids[3]));
	}

	isActive(player) {
		return Main.slowmoTimer > 0;
	}

	drawIcon(context, x, y) {
		Main.fg.textBaseline = "middle";
		Main.fg.textAlign = "center";
		Main.fg.font = Main.largeFont;
		Main.fg.fillStyle = this.color;
		Main.fg.fillText("S", x + 15, y + 15);

		Main.fg.textBaseline = "bottom";
		Main.fg.textAlign = "left";
	}
}

export class SuperPowerup extends Powerup {
	constructor(id) {
		super(id, ColorData.asteroids[3]);
	}

	apply(player) {
		player.powerupTime = 600;
		EntityManager.add(TextPopup, new TextPopup(player.pos.copy(), Lang.get("gameplay.supermode_popup"), this.color));
	}

	get color() {
		return `hsl(${Main.getRainbowHue()}, 80%, 60%)`;
	}

	set color(value) {}

	drawIcon(context, x, y) {
		Main.fg.textBaseline = "middle";
		Main.fg.textAlign = "center";
		Main.fg.font = Main.largeFont;
		Main.fg.fillStyle = this.color;
		Main.fg.fillText("S", x + 15, y + 15);

		Main.fg.textBaseline = "bottom";
		Main.fg.textAlign = "left";
	}
}