import { SlowmoPowerup, SuperPowerup } from "./powerup.js";

export const ColorData = {
	powerups: [0, 100, 50, 200, -1], //0=none, 1=explosion, 2=pierce, 3=slowmo, 4=super
	materials: [0],
	asteroids: ["hsl(30, 90%, 70%)", "hsl(0, 100%, 50%)", "not even a thing", "hsl(200, 70%, 70%)"],
	police: "hsl(0, 100%, 50%)",
	player: "#fff"
};

export const Data = {
	asteroidTypes: [0],
	materialValues: [100],
	powerups: [
		null,
		null,
		null,
		new SlowmoPowerup(3),
		new SuperPowerup(4)
	]
};

export const PowerupIDs = {
	slowmo: 3,
	superMode: 4
}

export const BulletIDs = {
	playerStandard: 0,
	playerExplosive: 1,
	playerPenetrate: 2,
	police1: 3,
	playerRainbow: 4
}