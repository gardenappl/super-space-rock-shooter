import { Vector } from "../vector.js";
import { Main } from "../main.js";
import { SeededRandom } from "../utils.js";
import { Star } from "../entities/star.js";

export class Sector {
	constructor(x, y, width = 2000, height = 2000, seed = new Date().getTime()) {
		this.stars = [];
		this.x = x;
		this.y = y;
		this.seed = seed + Math.abs(100000 * x) + Math.abs(1000 * y);
		this.width = width;
		this.height = height;
	}

	initStars(count, parallaxMin = Main.starParallaxMin, parallaxMax = Main.starParallaxMax, sparkleRate = Main.starSparkleRate) {
		this.stars = [];
		let rng = new SeededRandom(this.seed);
		for(var i = 0; i < count; i++) {
			let star = new Star(new Vector(this.x * this.width + rng.nextInt(0, this.width), this.y * this.height + rng.nextInt(0, this.height)),
					rng.nextFloat() < sparkleRate ? rng.nextInt(300, 1000) : -1, rng.nextFloat(parallaxMin, parallaxMax), rng.nextFloat(0.4, 1));
			star.twinkleTime = Math.floor(star.twinklePeriod * rng.nextFloat());
			this.stars.push(star);
		}
	}
}
