export class Mission {
	constructor(goal, ...restrictions) {
		this.goal = goal;
		this.restrictions = restrictions;
		this.complete = false;
		this.lastComplete = false;
		this.failed = false;
		this.lastFailed = false;
	}

	update() {
		if(this.complete)
			return;

		for(var i = this.restrictions.length - 1; i >= 0; i--) {
			if(this.restrictions[i].failed) {
				this.failed = true;
			}
		}

		if(!this.failed && this.goal.complete) {
			let complete = true;
			for (var i = this.restrictions.length - 1; i >= 0; i--) {
				if(!this.restrictions[i].canCompleteNow) {
					complete = false;
					break;
				}
			}
			if(complete) {
				this.complete = true;
			}
		}
	}
}

class MissionGoal {
	get complete() {
		return false;
	}

	printInfo() {
		return "NO MISSION GOAL, THIS IS A BUG";
	}
}

class GoalGetMoney extends MissionGoal {
	constructor(count) {
		super();
		this.count = count;
	}

	get complete() {
		return Main.player.money >= this.count;
	}

	printInfo() {
		return Lang.format("gameplay.mission.money.info", Main.player.money, this.count);
	}
}

class MissionRestriction {
	get failed() {
		return false;
	}

	get canCompleteNow() {
		return true;
	}

	printInfo() {
		return "EMPTY RESTRICTION, THIS IS A BUG";
	}
}

class RestrictionTimer extends MissionRestriction {
	constructor(time) {
		super();
		this.time = time;
	}

	get failed() {
		return Main.player.playTime > this.time;
	}

	get canCompleteNow() {
		return !this.failed;
	}

	printInfo() {
		if(!this.failed) {
			let ticks = this.time - Main.player.playTime;
			let seconds = Math.floor(ticks / 60);
			return Lang.format("gameplay.mission.time.info", seconds);
		} else {
			return Lang.get("gameplay.mission.time.fail");
		}
	}
}

export const MissionHandler = {
	init() {
		this.currentMission = null;
		this.completed = 0;
	},
	generateMission() {
		// this.currentMission = new Mission(
		// 	new GoalGetMoney(2000),
		// 	new RestrictionTimer(2000)
		// );
		return this.currentMission;
	},
	update() {
		if(this.currentMission == null) {
			return;
		}
		this.currentMission.update();
		if(this.currentMission.complete && !this.currentMission.lastComplete) {
			// Main.paused = true;
			alert("Mission Complete!");
			// Main.paused = false;
			this.completed++;
			this.currentMission.lastComplete = true;
			// this.currentMission = null;
		}
		else if(this.currentMission.failed && !this.currentMission.lastFailed) {
			alert("Mission failed");
			this.currentMission.lastFailed = true;
			// this.currentMission = null;
		}
	}
};