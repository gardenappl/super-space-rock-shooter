import { Main, Options } from "../main.js";
import { Career } from "./career.js";

export const SaveManager = {
	init() {
		try {
        	localStorage.setItem("x", "testing storage");
        	localStorage.removeItem("x");
        	this.available = true;
    	}
    	catch(e) {
	        this.available = false;
	    }
	},
	save() {
		if(!this.available) {
			return;
		}
		localStorage.clear();
		localStorage.setItem("aaay hello!", "You can cheat if you feel like, that's fine.");
		localStorage.setItem("control", Options.control);
		localStorage.setItem("starso", Main.starsOption);
		localStorage.setItem("shake", Options.enableShake);
		localStorage.setItem("shadows", Options.enableShadows);
		localStorage.setItem("dusts", Options.dusts);
		localStorage.setItem("mute", Options.mute);
		localStorage.setItem("money", Career.money);
		localStorage.setItem("firstlaunch", Main.firstLaunch);
	},
	load() {
		if(!this.available || !localStorage.getItem("aaay hello!")) {
			return false;
		}
		Main.firstLaunch = localStorage.getItem("firstlaunch") === "true";
		Options.control = parseInt(localStorage.getItem("control"));
		Main.starsOption = parseInt(localStorage.getItem("starso"));
		Options.enableStars = Main.starsOption > 0;
		Options.fancyStars = Main.starsOption == 2;
		Options.enableShake = localStorage.getItem("shake") === "true";
		Options.enableShadows = localStorage.getItem("shadows") === "true";
		Options.dusts = parseInt(localStorage.getItem("dusts"));
		Options.mute = localStorage.getItem("mute") == "true";
		Career.money = parseInt(localStorage.getItem("money"));
		return true;
	}
};