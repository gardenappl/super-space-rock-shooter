import { English } from "./english.js";
import { Russian } from "./russian.js";
import { Utils } from "../utils.js";

export const Lang = {
	uppercaseFont: false,
	currentLang: English,
	
	get(key, lang = Lang.currentLang) {
		if(!(key instanceof Array))
			key = key.split(".");
		let value = lang[key[0]];
		
		if(value === undefined) {
			return "[ERROR]";
		}
		
		for(var i = 1; i < key.length; i++) {
			value = value[key[i]];
			
			if(value === undefined) {
				return "[ERROR]";
			}
		}
		
		if(this.uppercaseFont) {
			return value.toString().toUpperCase();
		} else {
			return value.toString();
		}
	},
	format(key, ...args) {
		return Utils.format(this.get(key), args);
	}
};
