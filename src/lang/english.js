export const English = Object.freeze({
	menu: {
		title: "Super Space Rock Shooter",
		start: "Launch",
		select_player: "Select Player",
		ok: "OK",
		shop: "Shop",
		career: "Play",
		to_main_menu: "Back to Main Menu",
		splash_screen_start: "Start"
	},
	options: {
		toString() { return "Options" },
		control: {
			keyboard: "Control: Keyboard",
			mouse: "Control: Mouse"
		},
		stars: {
			enabled: "Stars: Glow",
			disabled: "Stars: Disabled",
			fast: "Stars: No Glow"
		},
		shake: {
			enabled: "Camera Shake: Enabled",
			disabled: "Camera Shake: Disabled"
		},
		shadows: {
			enabled: "Entity Glow: Enabled",
			disabled: "Entity Glow: Disabled"
		},
		dust: {
			enabled: "Particles: Glow",
			disabled: "Particles: Disabled",
			fast: "Particles: No Glow"
		},
		sound: {
			disabled: "Sound: Off",
			enabled: "Sound: On"
		}
	},
	gameplay: {
		player_classic: "Classic",
		player_backshoot: "Backshoot",
		speed: "Speed: {0} m/s",
		money: "Money: {0} Space Bucks",
		cargo: "Cargo: {0}/{1} {2}",
		material0: "Iron",
		cheat: "Cheat Mode!",
		dead: "GAME OVER",
		dead2: "50% of money lost",
		dead3: "Money received: {0} Space Bucks",
		paused: "PAUSED",
		time_limit: "Time left: {0}",
		mission: {
			fail: "Failed!",
			success: "Complete!",
			money: {
				desc: "Collect {0} Space Bucks",
				info: "{0} out of {1} Space Bucks",
			},
			time: {
				desc: "Do it in {0} seconds",
				info: "Time left: {0}",
				fail: "Out of time"
			}
		},
		boost: "Boost (ignore this)",
		powerup_hud: "Power:",
		slowmo: "Slow Motion",
		slowmo_popup: "Slow Motion!",
		supermode: "Hyper Mode",
		supermode_popup: "Hyper Mode!",
		powerup_actual_hud: "Hyper Mode available!",
		hit_taken: "Shield Down!"
	},
	debug: {
		fps: "{0} fps",
		entities: "Entities: {0}"
	},
	save: {
		unavailable1: "Warning: Local Storage is not available.",
		unavailable2: "Your game progress will not get saved.",
		unavailable3: "Update your broswer or check your settings."
	},
	credits: {
		game: "Code and sound by gardenapple - GPLv3",
		music1: "Gameplay music by Podington Bear - CC BY-SA",
		music2: "Menu music by Barry van Oudtshoorn (barryvan) - barryvan.com.au"
	}
});
