import { Vector } from "./vector.js";

export const Utils = {
    random(num1, num2) {
        if(num1 === undefined)
            num1 = Math.random();
        if(num2 === undefined) 
            return Math.random() * num1;
        return num1 + Math.random() * (num2 - num1);
    },

    randomInt(num1, num2) {
        if(num2 === undefined) 
            return Math.floor(Math.random() * num1);
        return Math.floor(num1 + Math.random() * (num2 - num1));
    },

    randomBool(num1, num2) {
        if(num1 === undefined)
            return Math.random() < 0.5;
        if(num2 === undefined)
            return Math.random() < 1 / num1;
        return Math.random() < num1 / num2;
    },

    randomVectorCircle(radius1, radius2) {
        if(radius2 === undefined)
            return Vector.fromAngle(Math.random() * Math.PI * 2, radius1);
        return Vector.fromAngle(Math.random() * Math.PI * 2, radius1 + Math.random() * (radius2 - radius1));
    },

    randomVectorSquare(x, y = x) {
        return new Vector(-x * 0.5 + Math.random() * x, -y * 0.5 + Math.random() * y);
    },

    randomVectorInRect(x1, y1, x2, y2) {
        if(x2 === undefined)
            return new Vector(Math.random() * x1, Math.random() * y1);
        return new Vector(Math.random() * (x2 - x1) + x1, Math.random() * (y2 - y1) + y1);
    },

    floorVector(vector) {
        return new Vector(Math.floor(vector.x), Math.floor(vector.y));
    },

    smoothStep(start, end, step, max = Math.abs(end - start)) {
        let change = (end - start) * step;
        if(change > 0) {
            if(change < max)
                return start + change;
            return start + max;
        }
        if(-change < max)
            return start + change;
        return start - max;
    },

    smoothAngleStep(start, end, step, max = end - start) {
        var delta = end - start;
        if(delta > Math.PI)
            return this.smoothStep(start + Math.PI * 2, end, step, max);
        if(delta < -Math.PI)
            return this.smoothStep(start - Math.PI * 2, end, step, max);
        return this.smoothStep(start, end, step, max);
    },
    
    smoothVectorStep(start, end, step, max = undefined) {
    	return new Vector(this.smoothStep(start.x, end.x, step, max), this.smoothStep(start.y, end.y, step, max));
    },

    clamp(value, min, max) {
        if(value < min)
            return min;
        if(value > max)
            return max;
        return value;
    },

    formatAngle(angle) {
        if(angle < -Math.PI) {
            return angle + Math.PI * Math.ceil((-angle - Math.PI) / Math.PI);
        }
        if(angle > Math.PI) {
            return angle - Math.PI * Math.ceil((angle - Math.PI) / Math.PI);
        }
        return angle;
    },

    // vectorOperationWithWrap: function(pos, wrapSize, margin, f) {
    //     f(pos.copy());
    //     if(pos.y < margin) {
    //         f(Vector.add(pos, new Vector(0, wrapSize.y)));
    //         if(pos.x < margin)
    //             f(Vector.add(pos, new Vector(wrapSize.x, wrapSize.y)));
    //         else if(pos.x > wrapSize.x - margin)
    //             f(Vector.add(pos, new Vector(-wrapSize.x, wrapSize.y)));
    //     }
    //     else if(pos.y > wrapSize.y - margin) {
    //         f(Vector.add(pos, new Vector(0, -wrapSize.y)));
    //         if(pos.x < margin)
    //             f(Vector.add(pos, new Vector(wrapSize.x, -wrapSize.y)));
    //         else if(pos.x > wrapSize.x - margin)
    //             f(Vector.add(pos, new Vector(-wrapSize.x, -wrapSize.y)));
    //     }
    //     if(pos.x < margin) 
    //         f(Vector.add(pos, new Vector(wrapSize.x, 0)));
    //     else if(pos.x > wrapSize.x - margin)
    //         f(Vector.add(pos, new Vector(-wrapSize.x, 0)));
    // },

    rotate(v, angle, pivot = Vector.zero) {
        let sin = Math.sin(angle);
        let cos = Math.cos(angle);
        return new Vector(cos * (v.x - pivot.x) - sin * (v.y - pivot.y) + pivot.x,
                          sin * (v.x - pivot.x) + cos * (v.y - pivot.y) + pivot.y);
    },

    format(string, args) {
        return string.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    }
};

export class SeededRandom {
    constructor(seed = Math.floor(Math.random() * 2147483647)) {
        this.seed = seed;
    }

    nextInt(num1, num2) {
        this.seed = this.seed * 48271 % 2147483647;
        if(num1 === undefined)
            return this.seed;
        else if(num2 === undefined)
            return Math.floor(this.seed / 2147483648 * num1);
        return num1 + Math.floor(this.seed / 2147483648 * (num2 - num1));
    }

    nextFloat(num1, num2) {
        this.seed = this.seed * 48271 % 2147483647;
        if(num1 === undefined)
            return this.seed / 2147483648;
        if(num2 === undefined)
            return this.seed / 2147483648 * num1;
        return num1 + this.seed / 2147483648 * (num2 - num1);
    }
}
