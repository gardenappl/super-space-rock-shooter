import { Main } from "../main.js";

export class GameState {
	constructor() {
	}

	init() {
		this.firstUpdate = true;
		Main.menuElements = [];
	}

	update() {
	}

	draw() {}

	quit() {}
}