import { GameState } from "./state.js";
import { MainMenu } from "./menu/main-menu.js";
import { Main, ScaleType, Options, ControlType, Controls } from "../main.js";
import { Vector } from "../vector.js";
import { Utils } from "../utils.js";
import { Police } from "../entities/police.js";
import { Asteroid } from "../entities/asteroid.js";
import { EntityManager } from "../entities/entity.js";
import { Player } from "../entities/player.js";
import { PlayerClassic } from "../entities/player-classic.js";
import { Dust } from "../entities/dust.js";
import { Pickup } from "../entities/pickup.js";
import { Bullet } from "../entities/bullet.js";
import { Career, Levels } from "../game/career.js";
import { MissionHandler } from "../game/mission.js";
import { Data, ColorData } from "../game/data.js";
import { SaveManager } from "../game/save.js";
import { Lang } from "../lang/lang.js";
import { SoundManager, Sounds } from "../game/sound.js";

export class StateGameplay extends GameState {
	constructor(level, player) {
		super();
		this.level = level;
		this.playerType = player;
	}

	init() {
		super.init();
		Main.currentLevel = Levels[this.level];
		Main.rngSeed = new Date().getTime();
		Main.firstLaunch = false;

		Main.width = Main.realScreenWidth * 2;
		Main.height = Main.realScreenHeight * 2;

		EntityManager.reset();
		if(this.playerType == 0)
			Main.player = new PlayerClassic(new Vector(Main.width / 2, Main.height / 2));
		else
			Main.player = new Player();
		EntityManager.add(Player, Main.player);

		//deprecated stuff below, use EntityManager
		Main.asteroids = EntityManager.entityMap.get(Asteroid);
		Main.dusts = EntityManager.entityMap.get(Dust);
		Main.bullets = EntityManager.entityMap.get(Bullet);
		Main.pickups = EntityManager.entityMap.get(Pickup);
		//deprecated stuff above

		Main.levelPlayTime = 0;
		Main.infiniteMode = true;
		Main.cheatMode = false;
		Main.resetSectors();
		Main.starParallaxMin = 0.65;
		Main.starParallaxMax = 0.95;
		Main.starCount = 0.001;
		Main.starSparkleRate = 0.5;
		Main.slowmoTimer = 0;
		Main.cameraShakeIntensity = 0;

		Main.bgCanvas.width = Main.screenWidth;
		Main.bgCanvas.height = Main.screenHeight;

		Main.bg.fillStyle = "hsl(300, 10%, 5%)";
		Main.bg.fillRect(0, 0, Main.screenWidth, Main.screenHeight);
		
		let bgGradient = Main.bg.createRadialGradient(Main.screenWidth / 2, Main.screenHeight / 2, 0, Main.screenWidth / 2, Main.screenHeight / 2, 500);
		bgGradient.addColorStop(0, `hsla(180, 50%, 50%, 0.2)`);
		bgGradient.addColorStop(1, "hsla(300, 10%, 5%, 0)");
		Main.bg.fillStyle = bgGradient;
		Main.bg.fillRect(0, 0, Main.screenWidth, Main.screenHeight);

		Main.oldCameraOffset = new Vector();
		Main.cameraOffset = new Vector();

		Main.currentMenuSong = null;
		Main.menuColor = null;

		let gameplaySongs = [ 
			Sounds.gameplaySong1,
			Sounds.gameplaySong2,
			Sounds.gameplaySong3,
			Sounds.gameplaySong4,
			Sounds.gameplaySong5,
		];
		SoundManager.playMusics(gameplaySongs, "gameplay");
		MissionHandler.generateMission();
	}

	update() {
		"use strict";
		super.update();
		// console.log("update");

		Main.levelPlayTime++;

		Main.setScaling(ScaleType.gameplay);
		Main.screenPos = Vector.subtract(Main.player.pos, new Vector(Main.screenWidth / 2, Main.screenHeight / 2));

		// if(Main.paused && Main.player.dead == -1)
		// 	return;

		Main.currentSector = new Vector(Math.floor(Main.player.pos.x / Main.screenWidth), Math.floor(Main.player.pos.y / Main.screenHeight));
		Main.updateSectors();
		if(Options.enableStars) {
			Main.updateStars();
		}


		// if(Options.control == ControlType.mouse)
		// 	Main.fgCanvas.style.cursor = "none";
		// else
		// 	Main.fgCanvas.style.cursor = "default";

		// console.log(Main.player.pos);

		let difficultyFactor = 0;
		if(Main.player.cargo > 10) //money = cargo * 100
			difficultyFactor = Utils.clamp(Math.pow(Main.player.cargo + 40, 0.8) / 140, 0, 1);

		if(Math.random() < 0.01 * difficultyFactor && EntityManager.getAll(Police).length < 0.5 + difficultyFactor) {
			let amount = Utils.random(1, 1);
			for(var i = 0; i < amount; i++) {
				let x = Math.random() < 0.5 ? Utils.random(Main.screenWidth / 2, Main.screenWidth / 2 + 100) : Utils.random(-Main.screenWidth / 2, -Main.screenWidth / 2 - 100);
				let y = Math.random() < 0.5 ? Utils.random(Main.screenHeight / 2, Main.screenHeight / 2 + 100) : Utils.random(-Main.screenHeight / 2, -Main.screenHeight / 2 - 100);
				EntityManager.add(Police, new Police(Vector.add(Main.player.pos, new Vector(x, y))));
			}
		}

		if(Math.random() < 0.02 && Main.asteroids.length < 10) {
			let pos = new Vector();
			let sides = [0, 1, 2, 3];
			if(Main.player.velocity.y < 0 && Math.abs(Main.player.velocity.y) > Math.abs(Main.player.velocity.x)) {
				// console.log("up");
				sides.push(0);
				sides.push(0);
			}
			else if(Main.player.velocity.x > 0 && Math.abs(Main.player.velocity.x) > Math.abs(Main.player.velocity.y)) {
				// console.log("right");
				sides.push(1);
				sides.push(1);
			}
			else if(Main.player.velocity.y > 0 && Math.abs(Main.player.velocity.y) > Math.abs(Main.player.velocity.x)) {
				// console.log("down");
				sides.push(2);
				sides.push(2);
			}
			else if(Main.player.velocity.x < 0 && Math.abs(Main.player.velocity.x) > Math.abs(Main.player.velocity.y)) {
				// console.log("left");
				sides.push(3);
				sides.push(3);
			}
			switch(sides[Utils.randomInt(sides.length)]) {
				case 0:
					pos = Vector.add(Main.screenPos, new Vector(Utils.random(0, Main.screenWidth), -300));
					break;
				case 1:
					pos = Vector.add(Main.screenPos, new Vector(Main.screenWidth + 300, Utils.random(-300, Main.screenHeight + 300)));
					break;
				case 2:
					pos = Vector.add(Main.screenPos, new Vector(Utils.random(0, Main.screenWidth), Main.screenHeight + 300));
					break;
				case 3:
					pos = Vector.add(Main.screenPos, new Vector(-300, Utils.random(-300, Main.screenHeight + 300)));
					break;
			}
			let amount = Utils.randomInt(1, 5);
			for(var i = 0; i < amount; i++) {
				let rand = Utils.random();
				let asteroidType = 0;
				if(rand < 0.02) {
					asteroidType = 1;
				} else if(rand < 0.03) {
					asteroidType = 3;
				}
				// alert(asteroidType);
				let asteroidPos = Vector.add(pos, Utils.randomVectorSquare(250));
				let velocity = Utils.randomVectorSquare(3);
				let asteroidRadius = Utils.random(30, 50);
				// rand = Utils.random();
				// if(rand < 0.015) {
				// 	let targetPos = Utils.randomVectorSquare(350).add(Main.player.pos);
				// 	if(Main.player.pos.copy().subtract(targetPos).length < 40)
				// 		targetPos.add(Utils.randomVectorCircle(100));
				// 	velocity = Vector.subtract(targetPos, asteroidPos);
				// 	velocity.length = Utils.random(2, 5);
				// 	asteroidRadius = Utils.random(20, 30);
				// }
				Main.asteroids.push(new Asteroid(asteroidPos, velocity, asteroidType, Main.currentLevel.material, asteroidRadius));
			}
		}
		// Main.dusts.push(new Dust(new Vector(Main.width / 2, Main.height / 2), Utils.randomVectorSquare(10)));
		// if(Main.stars.length < 1000)
		// 	Main.stars.push(new Star(Utils.randomVectorSquare(Main.screenWidth + 20, Main.screenHeight + 20).divide(0.8).add(Main.player.pos), Math.random() > 0.5 ? Utils.randomInt(300, 800) : -1, Utils.random(1, 2), Utils.random(0.7, 0.9)))

		EntityManager.update();
		EntityManager.clean();

		if(Main.slowmoTimer > 0)
			Main.slowmoTimer--;

		if(Main.player.dead > -1)
			Main.player.dead--;
		if(Main.player.dead == 1 || Main.player.playTime >= Career.timeLimit) {
			Main.nextQuit = true;
			Main.addMoney = true;
		}

		// console.log(EntityManager.getAll(Asteroid));

		MissionHandler.update();
	}

	draw() {
		"use strict";
		super.draw();
		// console.log("draw");

		Main.setScaling(ScaleType.gameplay);

		Main.screenPos = Vector.subtract(Main.player.getDrawPos(), new Vector(Main.screenWidth / 2, Main.screenHeight / 2));
		Main.oldCameraOffset = Main.cameraOffset;
		Main.cameraOffset = new Vector();

		Main.cameraShakeIntensity = 0;
		Main.cameraShakeIntensity = Main.player.velocity.length / 10;
		if(Main.player.accelerating) {
			Main.cameraShakeIntensity *= 1.5;
		}
		if(Main.player.shooting) {
			Main.cameraShakeIntensity *= 1.5;
		} else if(!Main.player.shooting && !Main.player.accelerating) {
			Main.cameraShakeIntensity = 0;
		}

		let cameraShake = new Vector();
		if(Options.enableShake && Main.player.dead == -1) {
			let shakeFreqX = 1.25;
  			let shakeFreqY = 0.75;
  			let shakeFreqY2 = 0.5;
  			let shakeSizeX = Main.cameraShakeIntensity;
  			let shakeSizeY = Main.cameraShakeIntensity / 2;
  			let shakeSizeY2 = Main.cameraShakeIntensity / 3;
  			let t = Main.drawTimestamp;
 			let xAdjustment = Math.sin( t*shakeFreqX )*shakeSizeX;
 			let yAdjustment = Math.sin( t*shakeFreqY )*shakeSizeY + Math.cos( t*shakeFreqY2 )*shakeSizeY2;

 			cameraShake = new Vector(xAdjustment, yAdjustment);
 			cameraShake = Utils.rotate(cameraShake, Main.player.rotation);
		}
		
		//if(Main.player instanceof PlayerClassic) {
			Main.cameraOffset.add(Vector.multiply(Main.player.velocity, 10));
		//}
		Main.cameraOffset = Utils.smoothVectorStep(Main.oldCameraOffset, Main.cameraOffset, 0.2);
		
		Main.screenPos.add(Main.cameraOffset).add(cameraShake);
		// Main.screenPos = new Vector(0, 0);

		Main.pickupLuma = (75 + Math.sin(Main.time / 10) * 25).toFixed(3);

		if(Options.enableStars) {
			Main.drawStars();
		}
		
		if(!Main.infiniteMode) {
			Main.fg.strokeStyle = "white";
			Main.fg.lineWidth = 15;
			Main.fg.strokeRect(-Main.screenPos.x, -Main.screenPos.y, Main.width, Main.height);
		}
		EntityManager.draw(Main.fg);

		Main.setScaling(ScaleType.ui);

		Main.fg.fillStyle = "white";
		Main.fg.font = Main.defaultFont;
		Main.fg.textBaseline = "top";
		Main.fg.textAlign = "left";
		let secondsTimeLeft = Math.floor((Career.timeLimit - Main.player.playTime) / 60);
		// Main.fg.fillText(Lang.format("gameplay.cargo", Main.player.cargo, Career.maxCargo, Lang.get("gameplay.material" + Main.currentLevel.material)), 20, 20);
		Main.fg.fillText(Lang.format("gameplay.money", Main.player.cargo * Main.materialValue[Main.currentLevel.material]), 20, 20);
		Main.fg.fillText(Lang.format("gameplay.speed", Main.player.velocity.length.toFixed(1)), 20, 40);

		if (Main.player.health <= 1 && !Options.debug) {
			Main.fg.fillText(Lang.get("gameplay.hit_taken"), 20, 70);
		}
		// Main.fg.fillText(Lang.format("gameplay.time_limit", secondsTimeLeft), 20, 60);

		// let luma = 50;
		// if(Main.player.powerupType == 2) {
		// 	if(Main.player.powerupTime > 300) {
		// 		luma = 100;
		// 	} else {
		// 		luma = Math.cos((300 - Main.player.powerupTime) / 10) * 25 + 75;
		// 	}
		// }
		// luma = luma.toFixed(3);

		if(Main.player.availablePowerup > 0) {
			Main.fg.fillStyle = `hsl(${Main.getRainbowHue()}, 80%, 60%)`;
			// Main.fg.fillStyle = "#fff";
			Main.fg.textAlign = "right";
			Main.fg.fillText(Lang.get("gameplay.powerup_actual_hud"), Main.screenWidth - 20, 20);
			Main.fg.textAlign = "left";
			// Main.fg.lineWidth = 2;
			// Main.fg.strokeStyle = "#fff";
			// Main.fg.strokeRect(Main.screenWidth - 45, 35, 30, 30);
			// Data.powerups[Main.player.availablePowerup].drawIcon(Main.fg, Main.screenWidth - 45, 35);
			// Main.fg.font = Main.defaultFont;
		}

		Main.fg.textBaseline = "bottom";
		Main.fg.font = Main.defaultFont;
		Main.fg.fillStyle = "#fff";

		// let text = Lang.get("gameplay.boost");
		// Main.fg.fillText(text, 20, Main.screenHeight - 25);

		// Main.fg.fillStyle = `hsl(50, 70%, 50%)`;
		// Main.fg.fillRect(20, Main.screenHeight - 20, Main.player.boost / 5, 10);
		// Main.fg.fillStyle = "white";
		// Main.fg.fillRect(20 + Main.player.boost / 5, Main.screenHeight - 20, Main.player.boostTimer / 70 * 100, 10);
		// Main.fg.strokeStyle = "white";
		// Main.fg.lineWidth = 2;
		// Main.fg.strokeRect(20, Main.screenHeight - 20, Main.player.maxBoost / 5, 10);
		// Main.fg.fillStyle = "white";


		if(Main.slowmoTimer > 0) {
			let text = Lang.get("gameplay.slowmo");
			Main.fg.fillText(text, 20, Main.screenHeight - 25);

			Main.fg.fillStyle = ColorData.asteroids[3];
			Main.fg.fillRect(20, Main.screenHeight - 20, Main.slowmoTimer / 200 * 200, 10);
			Main.fg.strokeStyle = "white";
			Main.fg.lineWidth = 2;
			Main.fg.strokeRect(20, Main.screenHeight - 20, 200, 10);
			Main.fg.fillStyle = "white";
			Main.fg.textBaseline = "top";
		}
		Main.fg.textBaseline = "top";

		if(Main.infiniteMode) {
			Main.fg.textAlign = "right";
			if(MissionHandler.currentMission != null) {
				Main.fg.fillText("Current Mission:", Main.screenWidth - 20, 20);
				let mission = MissionHandler.currentMission;
				Main.fg.fillText(mission.goal.printInfo(), Main.screenWidth - 20, 40);
				let y = 60;
				if(mission.complete) {
					Main.fg.fillText("- " + Lang.get("gameplay.mission.success"), Main.screenWidth - 20, y);
					y += 20;
				} else {
					for(var i = 0; i < mission.restrictions.length; i++) {
						Main.fg.fillText("- " + mission.restrictions[i].printInfo(), Main.screenWidth - 20, y);
						y += 20;
					}
				}
			}
			Main.fg.textAlign = "left";
		}
		Main.fg.fillStyle = "#fff";
		if(Main.cheatMode) {
			Main.fg.textAlign = "right";
			Main.fg.fillText(Lang.get("gameplay.cheat"), Main.screenWidth - 20, 40);
			Main.fg.textAlign = "left";
		}
		if(Options.debug) {
			Main.fg.fillText(Main.player.pos, 20, 80);
			Main.fg.fillText(Lang.format("debug.entities", Main.asteroids.length + Main.bullets.length + Main.pickups.length + Main.dusts.length), 20, 100);
			Main.fg.fillText("Dusts: " + Main.dusts.length, 20, 120);
			Main.fg.fillText("Bullets: " + Main.bullets.length, 20, 140);
			Main.fg.fillText("Asteroids: " + Main.asteroids.length, 20, 160);
			Main.fg.fillText("velocity: " + Main.player.velocity, 20, 180);
			Main.fg.fillText("Sounds: " + SoundManager._currentSFX.size, 20, 200);
		}
		if(Main.player.dead >= 0) {
			Main.fg.font = Main.largeFont;
			Main.fg.textBaseline = "middle";
			Main.fg.textAlign = "center";
			Main.fg.strokeStyle = "black";
			Main.fg.lineWidth = 2;
			Main.fg.strokeText(Lang.get("gameplay.dead"), Main.screenWidth / 2, Main.screenHeight / 2 - 25);
			Main.fg.fillText(Lang.get("gameplay.dead"), Main.screenWidth / 2, Main.screenHeight / 2 - 25);
			if(Main.player.dead < 250) {
				// Main.fg.strokeText(Lang.get("gameplay.dead2"), Main.screenWidth / 2, Main.screenHeight / 2 + 5);
				// Main.fg.fillText(Lang.get("gameplay.dead2"), Main.screenWidth / 2, Main.screenHeight / 2 + 5);
				Main.fg.strokeText(Lang.format("gameplay.dead3", Main.player.cargo * Main.materialValue[Main.currentLevel.material]), Main.screenWidth / 2, Main.screenHeight / 2 + 10);
				Main.fg.fillText(Lang.format("gameplay.dead3", Main.player.cargo * Main.materialValue[Main.currentLevel.material]), Main.screenWidth / 2, Main.screenHeight / 2 + 10);
			}
			Main.fg.textAlign = "left";
		} else if(Main.player.playTime > Career.timeLimit - 300) {
			Main.fg.font = Main.largeFont;
			Main.fg.textBaseline = "middle";
			Main.fg.textAlign = "center";
			Main.fg.strokeStyle = "black";
			Main.fg.lineWidth = 2;
			Main.fg.strokeText(Lang.format("gameplay.time_limit", secondsTimeLeft), Main.screenWidth / 2, Main.screenHeight / 2);
			Main.fg.fillText(Lang.format("gameplay.time_limit", secondsTimeLeft), Main.screenWidth / 2, Main.screenHeight / 2);
			Main.fg.textAlign = "left";
		}
		else if(Main.paused) {
			Main.fg.font = Main.largeFont;
			Main.fg.textBaseline = "middle";
			Main.fg.textAlign = "center";
			Main.fg.strokeStyle = "black";
			Main.fg.lineWidth = 2;
			Main.fg.strokeText("PAUSED", Main.screenWidth / 2, Main.screenHeight / 2);
			Main.fg.fillText("PAUSED", Main.screenWidth / 2, Main.screenHeight / 2);
			Main.fg.textAlign = "left";
		}
		if(Options.control == ControlType.mouse) {
			Main.setScaling(ScaleType.none);
			//this.drawMouse(Utils.floorVector(Controls.mousePos), Controls.mousePressed || Controls.keyShoot);
		}
		Main.setScaling(ScaleType.gameplay);
	}

	drawMouse(pos, pressed = false) {
		const radius = 10;
		const space = 2;
		Main.fg.strokeStyle = "#fff";
		Main.fg.lineWidth = 2;
		Main.fg.beginPath();


		if(pressed) {
			Main.fg.moveTo(pos.x - space, pos.y - radius);
			Main.fg.lineTo(pos.x - space, pos.y - space);
			Main.fg.lineTo(pos.x - radius, pos.y - space);

			Main.fg.moveTo(pos.x + space, pos.y - radius);
			Main.fg.lineTo(pos.x + space, pos.y - space);
			Main.fg.lineTo(pos.x + radius, pos.y - space);

			Main.fg.moveTo(pos.x + space, pos.y + radius);
			Main.fg.lineTo(pos.x + space, pos.y + space);
			Main.fg.lineTo(pos.x + radius, pos.y + space);

			Main.fg.moveTo(pos.x - space, pos.y + radius);
			Main.fg.lineTo(pos.x - space, pos.y + space);
			Main.fg.lineTo(pos.x - radius, pos.y + space);
		} else {
			Main.fg.moveTo(pos.x - space, pos.y - radius);
			Main.fg.lineTo(pos.x - radius, pos.y - radius);
			Main.fg.lineTo(pos.x - radius, pos.y - space);

			Main.fg.moveTo(pos.x + space, pos.y - radius);
			Main.fg.lineTo(pos.x + radius, pos.y - radius);
			Main.fg.lineTo(pos.x + radius, pos.y - space);

			Main.fg.moveTo(pos.x + space, pos.y + radius);
			Main.fg.lineTo(pos.x + radius, pos.y + radius);
			Main.fg.lineTo(pos.x + radius, pos.y + space);

			Main.fg.moveTo(pos.x - space, pos.y + radius);
			Main.fg.lineTo(pos.x - radius, pos.y + radius);
			Main.fg.lineTo(pos.x - radius, pos.y + space);
		}

		Main.fg.stroke();
	}

	quit() {
		if(Main.addMoney) {
			let addMoney = Main.player.cargo * Main.materialValue[Main.currentLevel.material];
			// if(Main.player.dead == 1) {
			// 	addMoney /= 2;
			// }
			Career.money += addMoney;
		}
		Main.addMoney = false;
		Main.currentLevel = null;
		SaveManager.save();
		Main.nextState = new MainMenu();
	}
}
