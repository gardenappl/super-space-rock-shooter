import { MenuState } from "./menu-state.js";
import { OptionsMenu } from "./options-menu.js";
import { GameMenuLaunch } from "./launch-menu.js";
import { Main } from "../../main.js";
import { MenuButton, MenuText, MenuList, ListElement } from "../../menu.js";
import { Vector } from "../../vector.js";
import { Lang } from "../../lang/lang.js";
import { SaveManager } from "../../game/save.js";

export class MainMenu extends MenuState {
	init() {
		super.init();

		Main.menuElements = [
			new MenuButton({
				pos: Vector.divide(Main.screenSize, 2).add(new Vector(-120, -30)),
				size: new Vector(240, 30),
				text: Lang.get(["menu", "career"]),
				onPress: function() {
					Main.nextState = new GameMenuLaunch();
				}
			}),
			new MenuText({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(0, 100)),
				text: Lang.get(["menu", "title"]),
				font: "bold " + Main.largeFont
			}),
			new MenuText({
				pos: Vector.subtract(Main.screenSize, new Vector(20, 60)),
				align: "right",
				textFunc() {
					return SaveManager.available ? "" : Lang.get("save.unavailable1");
				}
			}),
			new MenuText({
				pos: Vector.subtract(Main.screenSize, new Vector(20, 40)),
				align: "right",
				textFunc() {
					return SaveManager.available ? "" : Lang.get("save.unavailable2");
				}
			}),
			new MenuText({
				pos: Vector.subtract(Main.screenSize, new Vector(20, 20)),
				align: "right",
				textFunc() {
					return SaveManager.available ? "" : Lang.get("save.unavailable3");
				}
			}),
			new MenuButton({
				pos: Vector.divide(Main.screenSize, 2).add(new Vector(-120, 5)),
				size: new Vector(240, 30),
				text: Lang.get("options"),
				onPress() {
					Main.nextState = new OptionsMenu();
				}
			}),
			new MenuText({
				pos: new Vector(20, Main.screenHeight - 60),
				align: "left",
				textFunc() {
					return Lang.get("credits.game");
				}
			}),
			new MenuText({
				pos: new Vector(20, Main.screenHeight - 40),
				align: "left",
				textFunc() {
					return Lang.get("credits.music1");
				}
			}),
			new MenuText({
				pos: new Vector(20, Main.screenHeight - 20),
				align: "left",
				textFunc() {
					return Lang.get("credits.music2");
				}
			}),
		];
		Main.handleMouse();
	}
}
