import { MenuState } from "./menu-state.js";
import { MainMenu } from "./main-menu.js";
import { Main, Options, ControlType } from "../../main.js";
import { Vector } from "../../vector.js";
import { Lang } from "../../lang/lang.js";
import { MenuButton, MenuText } from "../../menu.js";
import { SaveManager } from "../../game/save.js";

export class OptionsMenu extends MenuState {
	init() {
		super.init();

		Main.menuElements = [
			new MenuText({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(0, 160)),
				text: Lang.get("options"),
				font: Main.largeFont
			}),
			new MenuText({
				pos: Vector.subtract(Main.screenSize, new Vector(20, 60)),
				align: "right",
				textFunc() {
					return SaveManager.available ? "" : Lang.get(["save", "unavailable1"]);
				}
			}),
			new MenuText({
				pos: Vector.subtract(Main.screenSize, new Vector(20, 40)),
				align: "right",
				textFunc() {
					return SaveManager.available ? "" : Lang.get("save.unavailable2");
				}
			}),
			new MenuText({
				pos: Vector.subtract(Main.screenSize, new Vector(20, 20)),
				align: "right",
				textFunc() {
					return SaveManager.available ? "" : Lang.get("save.unavailable3");
				}
			}),
			new MenuButton({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(120, 135)),
				size: new Vector(240, 30),
				textFunc() {
					return Lang.get(Options.mute ? "options.sound.disabled" : "options.sound.enabled");
				},
				onPress() {
					Options.mute = !Options.mute;
					SaveManager.save();
				}
			}),
			new MenuButton({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(120, 100)),
				size: new Vector(240, 30),
				textFunc() {
					return Lang.get(Options.enableShadows ? "options.shadows.enabled" : "options.shadows.disabled");
				},
				onPress() {
					Options.enableShadows = !Options.enableShadows;
					SaveManager.save();
				}
			}),
			new MenuButton({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(120, 65)),
				size: new Vector(240, 30),
				textFunc() {
					switch(Options.dusts) {
						case 0:
							return Lang.get("options.dust.disabled");
						case 1:
							return Lang.get("options.dust.fast");
						case 2:
							return Lang.get("options.dust.enabled");
					}
				},
				onPress(right) {
					if(right) {
						Options.dusts--;
						if(Options.dusts < 0)
							Options.dusts = 2;
					}
					else {
						Options.dusts++;
						if(Options.dusts > 2)
							Options.dusts = 0;
					}
					SaveManager.save();
				}
			}),
			new MenuButton({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(120, 30)),
				size: new Vector(240, 30),
				textFunc() {
					switch(Main.starsOption) {
						case 0:
							return Lang.get("options.stars.disabled");
						case 1:
							return Lang.get("options.stars.fast");
						case 2:
							return Lang.get("options.stars.enabled");
					}
				},
				onPress(right) {
					if(right) {
						Main.starsOption--;
						if(Main.starsOption < 0)
							Main.starsOption = 2;
					}
					else {
						Main.starsOption++;
						if(Main.starsOption > 2)
							Main.starsOption = 0;
					}

					switch(Main.starsOption) {
						case 0:
							Options.enableStars = false;
							Options.fancyStars = false;
							break;
						case 1:
							Options.enableStars = true;
							Options.fancyStars = false;
							break;
						case 2:
							Options.enableStars = true;
							Options.fancyStars = true;
							break;
					}
					SaveManager.save();
				}
			}),
			// new MenuButton({
			// 	pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(120, 30)),
			// 	size: new Vector(240, 30),
			// 	textFunc() {
			// 		switch(Options.control) {
			// 			case ControlType.mouse:
			// 				return Lang.get("options.control.mouse");
			// 			case ControlType.keyboard:
			// 				return Lang.get("options.control.keyboard");
			// 		}
			// 	},
			// 	onPress() {
			// 		Options.control++;
			// 		if(Options.control >= ControlType.count)
			// 			Options.control = 0;
			// 		SaveManager.save();
			// 	}
			// }),
			new MenuButton({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(120, -5)),
				size: new Vector(240, 30),
				textFunc() {
					switch(Options.enableShake) {
						case true:
							return Lang.get("options.shake.enabled");
						case false:
							return Lang.get("options.shake.disabled");
					}
				},
				onPress() {
					Options.enableShake = !Options.enableShake;
				}
			}),
			new MenuButton({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(120, -40)),
				size: new Vector(240, 30),
				text: Lang.get("menu.ok"),
				onPress() {
					SaveManager.save();
					Main.nextState = new MainMenu();
				}
			})
		];
		Main.handleMouse();
	}

	quit() {
		SaveManager.save();
		Main.nextState = new MainMenu();
	}
}