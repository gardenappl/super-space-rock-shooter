import { MenuState } from "./menu-state.js";
import { StateGameplay } from "../gameplay-state.js";
import { MainMenu } from "./main-menu.js";
import { MenuButton, MenuText, MenuEntityButton } from "../../menu.js";
import { Main } from "../../main.js";
import { Vector } from "../../vector.js";
import { Lang } from "../../lang/lang.js";
import { Career } from "../../game/career.js";
import { SaveManager } from "../../game/save.js";
import { Player } from "../../entities/player.js";
import { PlayerClassic } from "../../entities/player-classic.js";

export class GameMenuLaunch extends MenuState {
	init() {
		super.init();

		Main.menuElements = [
			new MenuText({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(0, 180)),
				text: Lang.get("menu.select_player"),
				font: Main.largeFont
			}),
			new MenuEntityButton({
				pos: Vector.divide(Main.screenSize, 2).add(new Vector(-250, -150)),
				size: new Vector(200, 200),
				entity: new PlayerClassic(),
				onPress() {
					Main.nextState = new StateGameplay(0, 0);
				}
			}),
			new MenuText({
				pos: Vector.divide(Main.screenSize, 2).add(new Vector(-150, -150 + 220)),
				textFunc() {
					return Lang.get("gameplay.player_classic");
				}
			}),
			new MenuText({
				pos: Vector.divide(Main.screenSize, 2).add(new Vector(150, -150 + 220)),
				textFunc() {
					return Lang.get("gameplay.player_backshoot");
				}
			}),
			new MenuEntityButton({
				pos: Vector.divide(Main.screenSize, 2).add(new Vector(50, -150)),
				size: new Vector(200, 200),
				entity: new Player(),
				onPress() {
					Main.nextState = new StateGameplay(0, 1);
				}
			})
		]
	}

	quit() {
		SaveManager.save();
		Main.nextState = new MainMenu();
	}
}
