import { GameState } from "../state.js";
import { Main, ScaleType, Options } from "../../main.js";
import { Utils } from "../../utils.js";
import { Vector } from "../../vector.js";
import { SoundManager, Sounds } from "../../game/sound.js";

export class MenuState extends GameState {
	init() {
		super.init();
		Main.setScaling(ScaleType.gameplay);
		Main.resetSectors();
		Main.starParallaxMin = 0.5;
		Main.starParallaxMax = 1;
		Main.starSparkleRate = 1;
		Main.starCount = 0.0005;
		// Main.fgCanvas.style.cursor = "default";
		if(Main.menuColor == null) {
			Main.menuColor = `hsla(${Utils.randomInt(0, 255)}, 70%, 30%, 0.6)`;
		}

		Main.bgCanvas.width = Main.screenWidth;
		Main.bgCanvas.height = Main.screenHeight;
		let bgGradient = Main.bg.createLinearGradient(Main.screenWidth / 2 - 100, Main.screenHeight / 2 - 500, Main.screenWidth / 2 + 100, Main.screenHeight / 2 + 500);
		bgGradient.addColorStop(0.2, "#000");
		bgGradient.addColorStop(0.5, Main.menuColor);
		bgGradient.addColorStop(0.8, "#000");


		Main.bg.fillStyle = bgGradient;
		Main.bg.fillRect(0, 0, Main.screenWidth, Main.screenHeight);
		Main.setScaling(ScaleType.ui);

		if(Main.currentMenuSong == null) {
			let song = Main.firstLaunch ? Sounds.menuSong1 : (Utils.randomBool() ? Sounds.menuSong1 : Sounds.menuSong2);
			SoundManager.playMusicLoop(song, Main.firstLaunch ? Sounds.menuSong1 : Utils.randomInt(0, 3) * 25.6);
			Main.currentMenuSong = song;
		}
		// console.log(id);
		// console.log(SoundManager._currentSFX[id]);
	}

	update() {
		super.update();
		Main.setScaling(ScaleType.gameplay);
		if(Options.enableStars) {
			Main.currentSector = new Vector(Math.floor(Main.screenPos.x / Main.screenWidth) + 1, Math.floor(Main.screenPos.y / Main.screenHeight) + 1);
			
			Main.updateSectors();
			Main.updateStars();
		}

		Main.screenPos.x += 0.5;
		Main.screenPos.y -= 0.1;
	}

	draw() {
		super.draw();
		Main.setScaling(ScaleType.gameplay);
		if(Options.enableStars) {
			Main.drawStars();
		}
	}
}
