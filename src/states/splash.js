import { Main } from "../main.js";
import { GameState } from "./state.js";
import { MenuButton } from "../menu.js";
import { MainMenu } from "./menu/main-menu.js";
import { Vector } from "../vector.js";
import { Lang } from "../lang/lang.js";

export class SplashScreen extends GameState {
	init() {
		super.init();
		Main.menuElements = [
			new MenuButton({
				pos: Vector.divide(Main.screenSize, 2).subtract(new Vector(120, 15)),
				size: new Vector(240, 30),
				text: Lang.get(["menu", "splash_screen_start"]),
				onPress: function() {
					Main.nextState = new MainMenu();
				}
			})
		];
		Main.handleMouse();
	}
}
