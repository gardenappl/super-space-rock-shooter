import { Vector } from "./vector.js";
import { SeededRandom } from "./utils.js";
import { SaveManager } from "./game/save.js";
import { MainMenu } from "./states/menu/main-menu.js";
import { SplashScreen } from "./states/splash.js";
import { StateGameplay } from "./states/gameplay-state.js";
import { Data, ColorData } from "./game/data.js";
import { EntityManager } from "./entities/entity.js";
import { Asteroid } from "./entities/asteroid.js";
import { Bullet } from "./entities/bullet.js";
import { Dust } from "./entities/dust.js";
import { Pickup } from "./entities/pickup.js";
import { Player } from "./entities/player.js";
import { Police } from "./entities/police.js";
import { Star } from "./entities/star.js";
import { TextPopup } from "./entities/text-popup.js";
import { SoundManager } from "./game/sound.js";
import { SpriteManager } from "./game/sprite.js";
import { Lang } from "./lang/lang.js";
import { Sector } from "./game/sky.js";

export const Controls = {
	keyLeft: false,
	keyRight: false,
	keyUp: false,
	keyDown: false,
	keyThrust: false,
	keyShoot: false,
	keyEnter: false,
	keyPowerup: false,
	mousePressed: false,
	middleMousePressed: false,
	rightMousePressed: false,
	mousePos: null,
	realMousePos: null
};

export const Options = {
	debug: false,
	pulsateBG: false,
	pulsateLast: false,
	control: 1,
	dusts: 2, //0 = disabled, 1 = no-glow, 2 = glow
	enableStars: true,
	fancyStars: true,
	fancyDust: true,
	uiScale: 1,
	gameplayScale: 1,
	enableShake: false,
	enableShadows: true,
	mute: false,
	animateStars: true
};

export const ScaleType = Object.freeze({
	none: 0,
	gameplay: 1,
	ui: 2
});

export const ControlType = Object.freeze({
	keyboard: 0,
	mouse: 1
});

export const Main = {
	init() {
		Main.menuElements = [];
		Main.menuColor = null;

		this.maxWidth = 800;
		this.maxHeight = 600;
		this.width = 0;
		this.height = 0;

		this.htmlCanvas = document.getElementById("fg");
		
		Main.offscreenCanvasSupported = "transferControlToOffscreen" in this.htmlCanvas;
		
		if(Main.offscreenCanvasSupported) {
			console.log("OffscreenCanvas is supported.");
			this.fgCanvas = this.htmlCanvas.transferControlToOffscreen();
		} else {
			console.log("OffscreenCanvas not supported! :(");
			this.fgCanvas = this.htmlCanvas;		
		}
		this.fg = this.fgCanvas.getContext("2d");
		

		const htmlBgCanvas = document.getElementById("bg");
		if(Main.offscreenCanvasSupported) {
			this.bgCanvas = htmlBgCanvas.transferControlToOffscreen();
		} else {
			this.bgCanvas = htmlBgCanvas;		
		}
		Main.bg = Main.bgCanvas.getContext("2d");

		Main.firstLaunch = true;
		Controls.mousePos = new Vector();
		Controls.realMousePos = new Vector();
		Main.realScreenWidth = this.maxWidth
		Main.realScreenHeight = this.maxHeight
		Main.realScreenSize = new Vector(Main.realScreenWidth, Main.realScreenHeight);
		Main.lastScale = 1;
		Main.setScaling(ScaleType.none);
		Main.fgCanvas.width = Main.realScreenWidth;
		Main.fgCanvas.height = Main.realScreenHeight;
		Main.screenPos = new Vector();
		Main.oldCameraOffset = new Vector();
		Main.cameraOffset = new Vector();
		Main.cameraShake = new Vector();
		Main.cameraShakeTimer = 5;
		Main.cameraShakeIntensity = 0;
		Main.defaultFont = "16px DejaVu Sans Mono, Liberation Mono, Courier, monospace";
		Main.largeFont = "24px DejaVu Sans Mono, Liberation Mono, Courier, monospace";
		Lang.uppercaseFont = false;
		Main.drawTimestamp = window.performance.now();
		Main.updateTimestamp = window.performance.now();
		Main.updateRate = 1000 / 60;
		Main.lastUpdateRate = 0;
		Main.glowBlur = 10;

		//misc
		Main.starsOption = 2;
		Main.totalRainbowBullets = 0;
		Main.currentMenuSong = null;

		//no right-click menu
		Main.htmlCanvas.addEventListener("contextmenu", event => event.preventDefault());

		Main.htmlCanvas.addEventListener("keydown", function(event) {
			switch(event.keyCode) {
				case 65: //a
				case 37: //left
					Controls.keyLeft = true;
					break;
				case 87: //w
				case 38: //up
					Controls.keyUp = true;
					break;
				case 68: //d
				case 39: //right
					Controls.keyRight = true;
					break;
				case 83: //s
				case 40: //down
					Controls.keyDown = true;
					break;
				case 76: //l
				case 90: //z
					Controls.keyShoot = true;
					break;
				case 80: //p
				case 88: //x
					Controls.keyThrust = true;
					break;
				case 73: //i
					Options.debug = !Options.debug;
					break;
				case 27: //esc
					// Main.nextState = new MainMenu();
					Main.nextQuit = true;
					break;
				case 13: //enter
				case 16: //shift
					Controls.keyPowerup = true;
					// Controls.keyEnter = true;
					// if(Main.state instanceof StateGameplay) {
					// 	Main.paused = !Main.paused;
					// }
					// break;
				// case 70:
				// 	toggleFullscreen();
				// 	break;
				// case 83:
				// 	Options.enableStars = !Options.enableStars;
				// 	break;
			}
		}, false);

		window.addEventListener("keyup", function(event) {
			switch(event.keyCode) {
				case 65: //a
				case 37: //left
					Controls.keyLeft = false;
					break;
				case 87: //w
				case 38: //up
					Controls.keyUp = false;
					break;
				case 68: //d
				case 39: //right
					Controls.keyRight = false;
					break;
				case 83: //s
				case 40: //down
					Controls.keyDown = false;
					break;
				case 76: //l
				case 90: //z
					Controls.keyShoot = false;
					break;
				case 80: //p
				case 88: //x
					Controls.keyThrust = false;
					break;
				case 13: //enter
				case 16: //shift
					Controls.keyPowerup = false;
					Controls.keyEnter = false;
					break;
			}
		}, false);

		window.addEventListener("mousedown", function(event) {
			switch(event.button) {
				case 0:
					Controls.mousePressed = true;
					break;
				case 1:
					Controls.middleMousePressed = true;
					break;
				case 2:
					Controls.rightMousePressed = true;
					break;
			}
		}, false);

		window.addEventListener("mouseup", function(event) {
			switch(event.button) {
				case 0:
					Controls.mousePressed = false;
					break;
				case 1:
					Controls.middleMousePressed = false;
					break;
				case 2:
					Controls.rightMousePressed = false;
					break;
			}
		}, false);

		window.addEventListener("mousemove", Main.updateMousePos, false);

		SpriteManager.init(() => { console.log("sprites loaded!"); Main.gameLoop(); });

		SaveManager.init();
		SaveManager.load();
		if(Main.firstLaunch)
			console.log("first launch, eh?");

		//register order determines draw order
		EntityManager.register(Dust);
		EntityManager.register(Player);
		EntityManager.register(Asteroid);
		EntityManager.register(Police);
		EntityManager.register(Bullet);
		EntityManager.register(Pickup);
		EntityManager.register(TextPopup);

		SoundManager.init(() => { console.log("sounds loaded!"); });

		Main.paused = false;
		Main.time = 0;

		Main.levelPlayTime = 0;
		Main.currentLevel = null;
		Main.currentSector = new Vector();
		Main.lastSector = 0;
		Main.sectors = [];
		Main.starParallaxMin = 0.65;
		Main.starParallaxMax = 0.95;
		Main.starSparkleRate = 0.5;
		Main.slowmoTimer = 0;
		Main.offLevelDespawnRadius = 400;

		Main.nextState = new SplashScreen();
		Main.nextQuit = false;
		Main.rngSeed = Date.now();

		//deprecated stuff below
		Main.powerupColors = ColorData.powerups;
		Main.materialColors = ColorData.materials;
		Main.materialValue = Data.materialValues;
		//deprecated stuff above

		Main.pickupLuma = 0;

		lastTimeFPS = new Date();
		frames = 0;
		Main.fps = 0;
		frameDelta = 0;
		lastFrameTime = 0;
		
		/*const stars = [];
		
		Options.fancyStars = true;
		for(let i = 0; i < 50; i++) {
			//i = x + width * y;
			const spriteX = i % 20;
			const spriteY = Math.floor(i / 20);
			stars.push(new Star(new Vector(10 + spriteX * 20, 60 + spriteY * 20), -1, 1, 0.2 + i / 25));
		}
		
		EntityManager.drawArray(Star, stars, Main.fg); */
	},

	gameLoop() {
		Main.update();
		requestAnimationFrame(function() {
			Main.draw();
			Main.gameLoop();
		});
	},

	update() {
		// console.log("requestAnimationFrame wait Main.time: " + (new Date() - lastFrameTime));
		// lastFrameTime = new Date();
		if(Main.nextQuit) {
			Main.state.quit();
			Main.nextQuit = false;
		}
		if(Main.nextState != null) {
			Main.state = Main.nextState;
			Main.state.init();
			Main.nextState = null;
		}

		Main.updateRate = 1000 / 60;
		if(Main.slowmoTimer > 0)
			Main.updateRate *= 2;

		if(Main.updateRate != Main.lastUpdateRate) {
			SoundManager.setGlobalRate((1000 / 60) / Main.updateRate);
			Main.lastUpdateRate = Main.updateRate;
		}


		let timestamp = window.performance.now();
		frameDelta += timestamp - lastFrameTime;
		// console.log(frameDelta);
		if(frameDelta > 300)
			frameDelta = 300;
		while(frameDelta > Main.updateRate) {
			// console.log(frameDelta);
			frameDelta -= Main.updateRate;
			if(!Main.paused) {
				Main.time++;
				Main.state.update();
				SoundManager.update();
				Main.state.firstUpdate = false;
				if(Main.nextQuit || Main.nextState != null)
					break;
			}
		}
		lastFrameTime = timestamp;

		Main.setScaling(ScaleType.ui);
		for(let i = 0; i < Main.menuElements.length; i++) {
			Main.menuElements[i].update();
		}



		// console.log("update Main.time: " + (new Date() - lastFrameTime));

		//measure fps
		Main.updateTimestamp = window.performance.now();
	},

	draw(timestamp) {
		frames++;
		let timePassed = window.performance.now() - lastTimeFPS;
		if(timePassed > 1000) {
			Main.fps = frames;
			lastTimeFPS = window.performance.now();
			frames = 0;
		}
		if(!Main.state.firstUpdate) {
			this.preStateDraw();
			Main.state.draw();
			this.postStateDraw();
		}
	},

	preStateDraw() {
		Main.drawTimestamp = window.performance.now();
		
		Main.fg.clearRect(0, 0, Main.screenWidth, Main.screenHeight);
	},

	getRainbowHue(initialHue = 0) {
		return (initialHue + (Main.drawTimestamp / 6)).toFixed() % 255;
	},

	postStateDraw() {
		Main.setScaling(ScaleType.ui);
		for(let i = 0; i < Main.menuElements.length; i++) {
			Main.menuElements[i].draw(Main.fg);
		}
	},

	setScaling(type) {
		Main.currentScale = type;
		Main.scale = 1;
		switch(type) {
			case ScaleType.gameplay:
				Main.scale = Options.gameplayScale;
				break;
			case ScaleType.ui:
				Main.scale = Options.uiScale;
				break;
		}
		Main.screenWidth = Main.realScreenWidth / Main.scale;
		Main.screenHeight = Main.realScreenHeight / Main.scale;
		Main.screenSize = Vector.divide(Main.realScreenSize, Main.scale);
		Main.fg.scale(Main.scale / Main.lastScale, Main.scale / Main.lastScale);
		Main.updateMousePos();
		Main.lastScale = Main.scale;
	},

	updateSectors() {
		if(!Main.currentSector.equals(Main.lastSector)) {
			let oldScale = Main.currentScale;
			Main.setScaling(ScaleType.gameplay);
			let newSectors = [];
			// console.log(Main.currentSector);
			for(var x = Main.currentSector.x - 1; x <= Main.currentSector.x + 1; x++) {
				sectorIterate: for(var y = Main.currentSector.y - 1; y <= Main.currentSector.y + 1; y++) {
					for(let i = 0; i < Main.sectors.length; i++) {
						if(Main.sectors[i].x == x && Main.sectors[i].y == y) {
							newSectors.push(Main.sectors[i]);
							// console.log(`copy: ${Main.sectors[i]}`);
							continue sectorIterate;
						}
					}
					let sector = new Sector(x, y, Main.screenWidth, Main.screenHeight, Main.rngSeed);
					newSectors.push(sector);
				}
			}
			Main.sectors = newSectors;
			Main.lastSector = Main.currentSector;
			Main.setScaling(oldScale);
		}
	},

	updateStars() {
		// console.log("update stars");
		for(let i = 0; i < Main.sectors.length; i++) {
			if(Main.sectors[i].stars.length == 0) {
				Main.sectors[i].initStars(Main.screenWidth * Main.screenHeight * Main.starCount);
			}
			if(Options.animateStars) {
				EntityManager.updateArray(Main.sectors[i].stars);
			}
		}
	},

	resetSectors() {
		Main.sectors = [];
		Main.currentSector = new Vector();
		Main.lastSector = null;
	},

	drawStars() {
		// console.log("draw stars");
		for(let i = 0; i < Main.sectors.length; i++) {
			EntityManager.drawArray(Star, Main.sectors[i].stars, Main.fg);
		}
	},

	updateMousePos(event) {
		if(event) {
			var rect = Main.htmlCanvas.getBoundingClientRect();
			Controls.realMousePos = new Vector((event.clientX - rect.left), (event.clientY - rect.top));
		}
		Controls.mousePos = Vector.divide(Controls.realMousePos, Main.scale);
		Main.handleMouse();
	},

	handleMouse() {
		for(let i = Main.menuElements.length - 1; i >= 0; i--) {
			Main.menuElements[i].checkMouseOver();
		}
	}
};

//FPS
var lastTimeFPS;
var frames;
var fps;
var frameDelta;
var lastFrameTime;

window.onload = function() {
	console.log("hi!");
	Main.init();
	
	
	//Main.gameLoop(); started after SpriteManager.load()
}
