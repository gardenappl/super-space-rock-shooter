export class Vector {
	constructor(x = 0, y = 0) {
		this.x = x;
		this.y = y;
	}

	copy() {
		return new Vector(this.x, this.y);
	}

	static fromAngle(angle, length = 1) {
		return new Vector(Math.cos(angle) * length, Math.sin(angle) * length);
	}

	toString() {
		return "x: " + this.x.toFixed(3) + " y: " + this.y.toFixed(3);
	}

	static distance(v1, v2) {
		return Vector.subtract(v1, v2).length;
	}

	static distanceSq(v1, v2) {
		return Vector.subtract(v1, v2).lengthSq;
	}

	get length() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}

	get lengthSq() {
		return this.x * this.x + this.y * this.y;
	}

	set length(value) {
		let angle = this.angle;
		this.x = Math.cos(angle) * value;
		this.y = Math.sin(angle) * value;
	}

	get angle() {
		return Math.atan2(this.y, this.x);
	}

	set angle(value) {
		let length = this.length;
		this.x = Math.cos(value) * length;
		this.y = Math.sin(value) * length;
	}

	static angleBetween(v1, v2) {
		return Math.arccos(Vector.dotProduct(v1, v2) / (v1.length * v2.length));
	}

	static angleBetweenCos(v1, v2) {
		return Vector.dotProduct(v1, v2) / (v1.length * v2.length);
	}

	static dotProduct(v1, v2) {
		return v1.x * v2.x + v1.y * v2.y;
	}

	static project(v1, v2) {
		return Vector.fromAngle(v2.angle, v1.length * Vector.angleBetweenCos(v1, v2));
	}

	add(v2) {
		this.x += v2.x;
		this.y += v2.y;
		return this;
	}

	static add(v1, v2) {
		return new Vector(v1.x + v2.x, v1.y + v2.y);
	}

	subtract(v2) {
		this.x -= v2.x;
		this.y -= v2.y;
		return this;
	}

	static subtract(v1, v2) {
		return new Vector(v1.x - v2.x, v1.y - v2.y);
	}

	multiply(num) {
		this.x *= num;
		this.y *= num;
		return this;
	}

	static multiply(v, num) {
		return new Vector(v.x * num, v.y * num);
	}

	multiplyBy(v2) {
		this.x *= v2.x;
		this.y *= v2.y;
		return this;
	}

	static multiplyBy(v1, v2) {
		return new Vector(v1.x * v2.x, v1.y * v2.y);
	}

	divide(num) {
		this.x /= num;
		this.y /= num;
		return this;
	}

	static divide(v, num) {
		return new Vector(v.x / num, v.y / num);
	}

	divideBy(v2) {
		this.x /= v2.x;
		this.y /= v2.y;
		return this;
	}

	static divideBy(v1, v2) {
		return new Vector(v1.x / v2.x, v1.y / v2.y);
	}

	equals(v2) {
		if(!(v2 instanceof Vector))
			return false;
		return this.x == v2.x && this.y == v2.y;
	}
}

Vector.zero = Object.freeze(new Vector()); //deprecated?