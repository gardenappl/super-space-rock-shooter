import { Main, Controls, Options } from "./main.js";
import { Vector } from "./vector.js";

export class MenuElement {
	constructor(x, y, width, height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.children = [];
		this.mouseOver = false;
		this.hide = false;
	}

	addChild(element) {
		this.children.push(element);
		element.parent = this;
	}

	checkMouseOver() {
		this.mouseOver = (Controls.mousePos.x > this.x && Controls.mousePos.x < this.x + this.width && Controls.mousePos.y > this.y && Controls.mousePos.y < this.y + this.height);
		for(let i = this.children.length - 1; i >= 0; i--)
			this.children[i].checkMouseOver();
	}

	update() {
		if(!this.hide) {
			this._update();
		}
		for(let i = this.children.length - 1; i >= 0; i--)
			this.children[i].update();
	}

	_update() {}

	draw(context) {
		if(!this.hide) {
			this._draw(context);
		}
		for(let i = this.children.length - 1; i >= 0; i--)
			this.children[i].draw(context);
	}

	_draw(context) {}
}

export class MenuPanel extends MenuElement {
	constructor({pos, size, radius = 0, text = "", textFunc, font = Main.defaultFont}) {
		super(pos.x, pos.y, size.x, size.y);
		this.radius = radius;
		this.color = "#999";
		if(text) {
			this.textFunc = function() {
				return text;
			}
		} else if(textFunc) {
			this.textFunc = textFunc;
		} else {
			this.textFunc = function() { return ""; };
		}
		this.textColor = "#fff";
		this.alpha = 0.4;
		this.font = font;
	}

	_draw(context) {
		context.globalAlpha = this.alpha;
		context.fillStyle = this.color;
		context.beginPath();
		context.moveTo(this.x, this.y + this.radius);
		context.arcTo(this.x, this.y, this.x + this.radius, this.y, this.radius);
		context.lineTo(this.x + this.width - this.radius, this.y);
		context.arcTo(this.x + this.width, this.y, this.x + this.width, this.y + this.radius, this.radius);
		context.lineTo(this.x + this.width, this.y + this.height - this.radius);
		context.arcTo(this.x + this.width, this.y + this.height, this.x + this.width - this.radius, this.y + this.height, this.radius);
		context.lineTo(this.x + this.radius, this.y + this.height);
		context.arcTo(this.x, this.y + this.height, this.x, this.y + this.height - this.radius, this.radius);
		context.closePath();
		context.fill();
		context.globalAlpha = 1;
		context.fillStyle = this.textColor;
		context.font = this.font;
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.fillText(this.textFunc(), this.x + this.width / 2, this.y + this.height / 2);
	}
}

export class MenuButton extends MenuPanel {
	constructor({pos, size, radius, onPress, text, textFunc, font = Main.defaultFont}) {
		super({pos, size, radius, text, textFunc, font});
		this.onPress = onPress;
		this.pressed = false;
		this.rightPressed = false;

		this.lastPressed = false;
		this.rightLastPressed = false;
		this.lastMouseOver = false;
	}

	_update() {
		const _lastPressed = this.pressed;
		const _rightLastPressed = this.rightPressed;
	
		if(this.mouseOver && !Controls.mousePressed && this.pressed) {
			this.onPress();
			this.pressed = false;
		}
		if(this.lastMouseOver && this.mouseOver && Controls.mousePressed && !this.pressed && !this.lastPressed) {
			this.pressed = true;
		}
		
		if(this.mouseOver && !Controls.rightMousePressed && this.rightPressed) {
			this.onPress(true);
			this.rightPressed = false;
		}
		if(this.lastMouseOver && this.mouseOver && Controls.rightMousePressed && !this.rightPressed && !this.rightLastPressed) {
			this.rightPressed = true;
		}
		
		if(!this.mouseOver) {
			this.pressed = false;
			this.rightPressed = false;
		}
		this.lastPressed = _lastPressed;
		this.rightLastPressed = _rightLastPressed;
		// if(this.mouseOver && !this.lastPressed && Controls.mousePressed) {
		// 	this.onPress();
		// }
	}

	_draw(context) {
		if(this.mouseOver && !this.lastMouseOver) {
			this.x -= 2;
			this.y -= 2;
			this.width += 4;
			this.height += 4;
			// this.textColor = "#000";
		} else if(!this.mouseOver && this.lastMouseOver) {
			this.x += 2;
			this.y += 2;
			this.width -= 4;
			this.height -= 4;
			// this.textColor = "#fff";
		}
		
		if((this.pressed || this.rightPressed) && !(this.lastPressed || this.rightLastPressed)) {
			this.x += 4;
			this.y += 4;
			this.width -= 8;
			this.height -= 8;
		} else if(!(this.pressed || this.rightPressed) && (this.lastPressed || this.rightLastPressed)) {
			this.x -= 4;
			this.y -= 4;
			this.width += 8;
			this.height += 8;
		}
		
		if(this.pressed || this.rightPressed) {
			this.alpha = 0.8;
		} else if(this.mouseOver) {
			this.alpha = 0.5;
		} else {
			this.alpha = 0.4;
		}
		this.lastMouseOver = this.mouseOver;
		super._draw(context);
	}
}

export class MenuText extends MenuElement {
	constructor({pos, text, textFunc, font = Main.defaultFont, color = "#fff", baseline = "middle", align = "center"}) {
		super(pos.x, pos.y, 0, 0);
		if(text) {
			this.textFunc = function() {
				return text;
			}
		} else {
			this.textFunc = textFunc;
		}
		this.font = font;
		this.color = color;
		this.baseline = baseline;
		this.align = align;
	}

	_draw(context) {
		context.fillStyle = this.color;
		context.font = this.font;
		context.textBaseline = this.baseline;
		context.textAlign = this.align;
		context.fillText(this.textFunc(), this.x, this.y, context.width);
	}
}

export class MenuEntityButton extends MenuButton {
	constructor({pos, size, radius, onPress, entity}) {
		super({pos, size, radius, onPress});
		this.entity = entity;
	}

	_draw(context) {
		super._draw(context);
		this.entity.draw(context, this.x + this.width / 2, this.y + this.height / 2);
	}

	_update() {
		super._update();
		this.entity.rotation += 0.05;
	}
}

export class ListElement {
	constructor(size, elements) {
		this.width = size.x;
		this.height = size.y;
		this.elements = elements;
	}

	draw(context, x, y) {
		context.save();
		context.translate(x, y);
		for (var i = 0; i < this.elements.length; i++) {
			this.elements[i].draw(context);
		}
		context.restore();
	}

	checkMouseOver(x, y) {
		for (var i = 0; i < this.elements.length; i++) {
			this.elements[i].x += x;
			this.elements[i].y += y;
			this.elements[i].checkMouseOver();
			this.elements[i].x -= x;
			this.elements[i].y -= y;
		}
	}

	update() {
		for (var i = 0; i < this.elements.length; i++) {
			this.elements[i].update();
		}
	}
}

export class MenuList extends MenuElement {
	constructor({pos, width, num, listElements}) {
		super(pos.x, pos.y, width, listElements[0].height * num + 50);
		this.height = listElements[0].height * num + 50;
		this.elementHeight = listElements[0].height;
		this.pages = Math.ceil(listElements.length / num);
		this.num = num;
		this.listElements = listElements;
		let list = this;
		this.addChild(new MenuButton({
			pos: new Vector(x, y),
			size: new Vector(width, 20),
			text: "↑",
			onPress() {
				list.currentPage--;
			},
			font: "24px Courier"
		}));
		this.addChild(new MenuButton({
			pos: new Vector(x, y + this.height - 20),
			size: new Vector(width, 20),
			text: "↓",
			onPress() {
				list.currentPage++;
			},
			font: "24px Courier"
		}));
		this.currentPage = 0;
	}

	_draw(context) {
		for(let i = 0; i < this.num; i++) {
			let index = this.currentPage * this.num + i;
			if(index < 0 || index >= this.listElements.length) {
				return;
			}
			this.listElements[index].draw(context, this.x, this.y + 25 + this.elementHeight * i);
		}
	}

	checkMouseOver() {
		super.checkMouseOver();
		for(let i = 0; i < this.num; i++) {
			let index = this.currentPage * this.num + i;
			if(index < 0 || index >= this.listElements.length) {
				return;
			}
			this.listElements[index].checkMouseOver(this.x, this.y + 25 + this.elementHeight * i);
		}
	}

	update() {
		this.children[0].hide = false;
		this.children[1].hide = false;
		if(this.currentPage == 0) {
			this.children[0].hide = true;
		}
		if(this.currentPage == this.pages - 1) {
			this.children[1].hide = true;
		}
		super.update();
		for(let i = 0; i < this.num; i++) {
			let index = this.currentPage * this.num + i;
			if(index < 0 || index >= this.listElements.length) {
				return;
			}
			this.listElements[index].update();
		}
	}
}
