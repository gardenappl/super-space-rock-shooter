import { Entity } from "./entity.js";
import { Vector } from "../vector.js";
import { Main, Options } from "../main.js";
import { SpriteManager, SpriteIDs } from "../game/sprite.js";

export class Star extends Entity {
	constructor(pos = new Vector(), twinkleTime = -1, parallax = 0.8, size = 1) {
		super(pos, new Vector());
		this.drawRadius = 5;
		this.twinklePeriod = twinkleTime;
		this.twinkleTime = twinkleTime;
		this.size = size;
		this.parallax = parallax;
		this.levelWrap = false;
	}

	update() {
		super.update();
		if(this.twinklePeriod != -1) {
			this.twinkleTime--;
			if(this.twinkleTime < 0)
				this.twinkleTime = this.twinklePeriod;
			// alert(this.twinkleTime);
		}
	}

	static preDraw(context) {
		context.fillStyle = "#fff";
		if(Options.fancyStars) {
			context.shadowBlur = Main.glowBlur * 0.5;
			context.shadowColor = context.fillStyle;
		}
	}

	_draw(context, drawX = (this.pos.x - Main.screenPos.x) * this.parallax, drawY = (this.pos.y - Main.screenPos.y) * this.parallax) {
		/* let sin = 0;
		if(Options.animateStars && this.twinkleTime != -1 && this.twinkleTime < 120) {
			sin = Math.sin(this.twinkleTime / 60 * Math.PI);
		}
		
		if(Options.fancyStars) {
			SpriteManager.sprites[SpriteIDs.starSheet + Math.floor(25 * (this.size * (1 + 0.5 * sin) - 0.2))].draw(context, drawX, drawY);
		} else {
			context.fillRect(drawX - (1 + 0.5 * sin) * this.size, drawY - (1 + 0.5 * sin) * this.size, (2 + sin) * this.size, (2 + sin) * this.size);
		} */
	
		if(!Options.animateStars || this.twinkleTime == -1 || this.twinkleTime > 120) {
			context.fillRect(drawX - this.size, drawY - this.size, 2 * this.size, 2 * this.size);
		}
		else {
			const sin = Math.sin(this.twinkleTime / 60 * Math.PI);
			context.fillRect(drawX - (1 + 0.5 * sin) * this.size, drawY - (1 + 0.5 * sin) * this.size, (2 + sin) * this.size, (2 + sin) * this.size);
		} 
	}

	static postDraw(context) {
		context.shadowBlur = 0;
	}

	shouldDraw() {
		let x = (this.pos.x - Main.screenPos.x) * this.parallax;
		let y = (this.pos.y - Main.screenPos.y) * this.parallax;
		return x > -this.drawRadius && x < Main.screenWidth + this.drawRadius && y > -this.drawRadius && y < Main.screenHeight + this.drawRadius;
	}
}
