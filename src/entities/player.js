import { Entity, EntityManager, EntityLiving } from "./entity.js";
import { Asteroid } from "./asteroid.js";
import { Pickup } from "./pickup.js";
import { Dust } from "./dust.js";
import { Police } from "./police.js";
import { Bullet } from "./bullet.js";
import { Vector } from "../vector.js";
import { TextPopup } from "./text-popup.js";
import { Lang } from "../lang/lang.js";
import { Main, Options, Controls, ControlType, ScaleType } from "../main.js";
import { Utils } from "../utils.js";
import { Data, ColorData, BulletIDs, PowerupIDs } from "../game/data.js";
import { SoundManager, Sounds } from "../game/sound.js";
// import { PlayerClassic } from "./player-classic.js";

export class Player extends EntityLiving {
	constructor(pos = new Vector(), velocity = new Vector()) {
		super(pos, velocity, 2);
		this.radius = 5;
		this.drawRadius = 10;
		this.shootDelay = 0;
		this.dustDelay = 5;
		// this.powerupType = 0;
		this.powerupTime = 0;
		this.money = 0;
		this.dead = -1;
		this.wallClip = 1;
		this.playTime = 0;
		this.cargo = 0;
		this.boost = 1000;
		this.maxBoost = 1000;
		this.boostTimer = 0;
		this.boostRegenTime = 0;
		this.boosting = false;
		this.lastBoosting = false;
		this.availablePowerup = 0;
		this.currentPowerup = null;
		this.powerupLastActive = false;
		this.superTimer = 0;
		this.accelerating = this.shooting = false;
		this.rainbowShootSpread = 2;
	}

	update() {
		// this.playTime++;
		if(this.shootDelay > 0)
			this.shootDelay--;

		if(this.currentPowerup != null && this.currentPowerup.isActive(this)) {
			this.currentPowerup.update(this);
			this.powerupLastActive = true;
		} else {
			if(this.powerupLastActive) {
				this.currentPowerup.lastUpdate(this);
			}
			this.currentPowerup = null;
			this.powerupLastActive = false;
		}

		let boosting = false;
		this.playerMovement();

		super.update();

		this.dustDelay--;
		if(this.dustDelay <= 0)
			this.dustDelay = 3;


		// if(boosting && this.boostTimer == 0 && this.boost >= 500) {
		// 	this.boost -= 500;
		// 	this.boostTimer = 70;
		// 	this.boosting = true;
		// } else if(this.boostTimer > 0) {
		if(boosting) {
			this.boosting = true;
			this.boostRegenTime = 120;
			this.boostTimer--;
			// if(this.boostTimer == 69) {
			// 	this.velocity.add(Vector.fromAngle(this.rotation, 4));
			// }
			// if(this.boostTimer > 40) {
			{
				this.velocity.add(Vector.fromAngle(this.rotation, 0.5));
			}
		} else {
			this.boosting = false;
		}
		if(this.powerupType == 2) {
			this.boost = this.maxBoost;
		}
		// this.velocity.add(Vector.fromAngle(this.rotation, acceleration));
		// if(acceleration > 0 && this.dustDelay == 1 && Utils.random(0, 0.1) < acceleration + 0.02) {
		// 	let shootPos = Vector.add(this.pos, Vector.fromAngle(this.rotation, -12).add(Vector.fromAngle(this.rotation + Math.PI / 2, Utils.random(-5, 5))));
		// 	let shootVel = Vector.fromAngle(this.rotation, -4 * acceleration / maxAcceleration).add(Utils.randomVectorSquare(2.5));
		// 	Main.dusts.push(new Dust(shootPos, shootVel, 0.5));
		// }

		if(this.boostRegenTime == 0) {
			this.boost = Math.min(this.boost + 1.5, this.maxBoost);
		} else {
			this.boostRegenTime--;
		}

		let asteroids = EntityManager.getAll(Asteroid, Police);
		for(var i = 0; i < asteroids.length; i++) {
			if(this.collides(asteroids[i])) {
				if((this.currentPowerup == null || this.currentPowerup.id != PowerupIDs.superMode) && !Main.cheatMode && !this.boosting)
					this.takeDamage(1, asteroids[i]);
				asteroids[i].die(this);
				return;
			}
		}
		let pickups = EntityManager.getAll(Pickup);
		let pickupRadius = this.boosting ? 400 : 90;
		let pickupSpeed = this.boosting ? 0.5 : 0.05;
		for(var i = 0; i < pickups.length; i++) {
			if(Vector.distance(this.pos, pickups[i].pos) < 90 || Main.cheatMode) {
				pickups[i].velocity.add(Vector.subtract(this.pos, pickups[i].pos).multiply(pickupSpeed));
			}
		}
		// for(var i = 0; i < Main.asteroids.length; i++) {
		// 	if(Vector.distance(this.pos, Main.asteroids[i].pos) < 500) {
		// 		let velocity = Vector.subtract(this.pos, Main.asteroids[i].pos).multiply(0.001);
		// 		if(velocity.length > 0.01)
		// 			velocity.length = 0.01;
		// 		Main.asteroids[i].velocity.add(velocity);
		// 	}
		// }
		this.lastBoosting = this.boosting;
	}

	playerMovement() {
		this.shooting = this.accelerating = Controls.keyShoot || (Options.control == ControlType.mouse && Controls.mousePressed);
		if(this.dead != -1) {
			this.accelerating = false;
			this.shooting = false;
		}
		// console.log(`powerup Main.time: ${this.powerupTime}, powerup type: ${this.powerupType}`);

		if(this.accelerating && this.shootDelay == 0) {
			if(this.boostTimer < 40) {
				let accel = 1;
				if(this.currentPowerup != null && this.currentPowerup.id == PowerupIDs.superMode) {
					accel = 0.1;
				}
				this.velocity.add(Vector.fromAngle(this.rotation, 1));
			}
			this.shoot(this.rotation + Math.PI, Vector.fromAngle(this.rotation, -12));
		}

		let acceleration = 0;
		// let boosting = false;
		// const maxAcceleration = 0.16; //minimal acceleration is 120 / 1500 = 2 / 25
		switch(Options.control) {
			case ControlType.mouse:
				let mouseVector = Vector.subtract(Vector.add(Controls.mousePos, Main.screenPos), this.pos).multiply(Main.scale);
				// this.rotation = Utils.smoothAngleStep(this.rotation, mouseVector.angle, 0.5, 1);
				this.rotation = mouseVector.angle;
				
				// if(mouseVector.length > 60) {
				// 	acceleration = Utils.clamp((mouseVector.length + 60) / 1500, 0, maxAcceleration);
				// } else {
				// 	acceleration = 0;
				// }
				if(Controls.keyPowerup || Controls.middleMousePressed) {
					// boosting = true;
					this.usePowerup();
				}
				break;
			case ControlType.keyboard:
				if(Controls.keyLeft)
					this.rotation -= 0.12;
				if(Controls.keyRight)
					this.rotation += 0.12;
				if(Controls.keyThrust) {
					boosting = true;
				}
				break;
		}
		this.velocity.multiply(0.975);
	}

	usePowerup() {
		if(this.availablePowerup > 0 && (this.currentPowerup == null || !this.currentPowerup.isActive(this))) {
			this.currentPowerup = Data.powerups[this.availablePowerup];
			this.currentPowerup.apply(this);
			this.availablePowerup = 0;
		}
	}

	shoot(direction, originPos) {
		if(this.currentPowerup != null && this.currentPowerup.id == PowerupIDs.superMode) {
			let shootVel = Vector.fromAngle(direction, 10);
			shootVel.add(this.velocity);
			let amount = (Main.time % 4 < 2) ? 3 : 4;
			for(var i = 0; i < amount; i++) {
				let shootVel2 = Utils.rotate(shootVel, ((Main.time % 4 < 2) ? -0.2 : -0.3) * this.rainbowShootSpread + i * 0.2 * this.rainbowShootSpread);
				let bullet = new Bullet(this.pos.copy().add(originPos), shootVel2, BulletIDs.playerRainbow);
				EntityManager.add(Bullet, bullet);
			}
			SoundManager.play(Sounds.shoot);
		} else {
			let shootVel = Vector.fromAngle(direction, 10);
			shootVel.add(this.velocity);
			let bullet = new Bullet(this.pos.copy().add(originPos), shootVel, 0);
			// if(this.powerupType == 2)
			// 	bullet.velocity.multiply(1.5);
			EntityManager.add(Bullet, bullet);
			SoundManager.play(Sounds.shoot);
		}

		if(Main.cheatMode) {
			this.shootDelay = 1;
		} else if(this.currentPowerup != null && this.currentPowerup.id == 4) {
			this.shootDelay = 3;
		} else {
			this.shootDelay = 5;
		}
	}

	die() {
		super.die();
		SoundManager.play(Sounds.deathPlayer);
		Main.slowmoTimer = 0;
		this.dead = 300;
		let amount = 10 + Math.random() * 10;
		for(var j = 0; j < amount; j++)
			EntityManager.add(Dust, new Dust(this.pos.copy(), Vector.add(this.velocity, Utils.randomVectorSquare(5)), 2, this.health > 1 ? ColorData.player : "#aaa"));
	}

	takeDamage(damage, attacker) {
		if(!super.takeDamage(damage, attacker))
			return false;

		if(this.currentPowerup != null && this.currentPowerup.id == PowerupIDs.superMode)
			return false;

		SoundManager.play(Sounds.hit, this.pos.copy());

		if(this.health == 1)
			EntityManager.add(TextPopup, new TextPopup(this.pos.copy(), Lang.get("gameplay.hit_taken"), ColorData.player));
		
		let amount = 25 + Math.random() * 5;
		for(var j = 0; j < amount; j++)
			EntityManager.add(Dust, new Dust(this.pos.copy(), Vector.add(this.velocity, Utils.randomVectorSquare(4)), 2, this.health > 1 ? ColorData.player : "#aaa"));
		return true;
	}

	_draw(context, drawX = this.pos.x - Main.screenPos.x, drawY = this.pos.y - Main.screenPos.y) {
		// return false;
		context.lineWidth = 2;
    	if(this.currentPowerup == null) {
			context.strokeStyle = this.health > 1 ? ColorData.player : "#aaa", "hsl(0, 40%, 90%)";
    	} else {
			let luma = 50;
			if(this.powerupTime < 300) {
				luma = 75 + Math.sin((50 - this.powerupTime) / 10) * 25;
				luma = luma.toFixed(3);
			}
			let hue = Main.powerupColors[this.powerupType];
			if(this.currentPowerup.id == 4)
				hue = Main.getRainbowHue();
			context.strokeStyle = `hsl(${hue}, 80%, ${luma}%)`;
		}
    	context.beginPath();
    	context.save();
  		context.translate(drawX, drawY); // pivot point
    	context.rotate(this.rotation);

		context.moveTo(11, 0);
		context.lineTo(-9, -7);
		context.lineTo(-12, 0);
		context.lineTo(-9, 7);
		context.closePath();

		if(this.boosting) {
			context.restore();
			context.stroke();
	    	context.save();
	  		context.translate(drawX, drawY); // pivot point
	    	context.rotate(this.rotation);
	    	context.beginPath();
			context.moveTo(0, -15);
			context.arc(0, 0, 15, -0.5 * Math.PI, 0.5 * Math.PI);
		}

    	context.restore();

		if(Options.enableShadows) {
			context.shadowBlur = Main.glowBlur;
			context.shadowColor = context.strokeStyle;
		}
    	context.stroke();
    	context.shadowBlur = 0;
	}

	static findNearest() { //placeholder for multiplayer?
		return Main.player;
	}
}
