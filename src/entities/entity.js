import { Vector } from "../vector.js";
import { Main } from "../main.js";
import { Utils } from "../utils.js";

let hello = false;

export class Entity {
	constructor(pos = new Vector(), velocity = new Vector()) {
		this.pos = pos;
		this.lastPos = pos.copy();
		this.velocity = velocity;
		this.radius = 1;
		this.rotation = Math.PI * 1.5;
		this.drawRadius = -1;
		this.timeLeft = -1;
		this.maxTimeLeft = -1;
		this.dieNextTick = false;
		this.levelWrap = true;
		this.insideLevel = false;
	}

	//deprecated shit below
	get x() {
		return this.pos.x;
	}

	set x(value) {
		this.pos.x = value;
	}

	get y() {
		return this.pos.y;
	}

	set y(value) {
		this.pos.y = value;
	}
	//deprecated shit above

	update() {
		if(this.dieNextTick) {
			this.die();
			return;
		}
		if(this.timeLeft == -1 && this.maxTimeLeft != -1)
			this.timeLeft = this.maxTimeLeft;
		if(this.timeLeft != -1) {
			this.timeLeft--;
			if(this.timeLeft <= 0) {
				this.die();
				return;
			}
		}
		this.ai();

		this.updatePosition();
	}

	ai() {}

	updatePosition() {
		this.lastPos = this.pos.copy();
		this.pos.add(this.velocity);

		// if(Main.currentLevel != null && !this.insideLevel) {
		// 	if(this.pos.x > 0 && this.pos.x <= Main.currentLevel.width && this.pos.y > 0 && this.pos.y <= Main.currentLevel.height) {
		// 		this.insideLevel = true;
		// 	}
		// }

		// if(Main.currentLevel != null && this.levelWrap && this.insideLevel)
		// {
		// 	if(this.pos.x < 0) {
		// 		this.pos.x = this.pos.x + Main.currentLevel.width;
		// 	} else if(this.pos.x >= Main.currentLevel.width) {
		// 		this.pos.x = this.pos.x - Main.currentLevel.width;
		// 	}
		// 	if(this.pos.y < 0) {
		// 		this.pos.y = this.pos.y + Main.currentLevel.height;
		// 	} else if(this.pos.y >= Main.currentLevel.height) {
		// 		this.pos.y = this.pos.y - Main.currentLevel.height;
		// 	}
		// }
	}

	getDrawPos() {
		let drawPos = this.lastPos;
		let interp = Utils.clamp((Main.drawTimestamp - Main.updateTimestamp) / Main.updateRate, 0, 1);
		drawPos.x = Utils.smoothStep(this.lastPos.x, this.pos.x, interp);
		drawPos.y = Utils.smoothStep(this.lastPos.y, this.pos.y, interp);
		if(!hello) {
			// alert(Main.updateRate, Main.drawTimestamp);
			hello = true;
		}

		return drawPos;
	}

	collides(entity, wrap = true) {
		// let width = Main.currentLevel.width;
		// let height = Main.currentLevel.height;

		let b = this.collidesAtPos(entity, this.pos);
		if(b) return b;
		// if(wrap) {
		// 	let margin = Math.max(this.radius, entity.radius);
	 //        if(this.pos.y < margin) {
	 //            b |= this.collidesAtPos(entity, Vector.add(this.pos, new Vector(0, height)));
		// 		if(b) return b;
	 //            if(this.pos.x < margin)
	 //                b |= this.collidesAtPos(entity, Vector.add(this.pos, new Vector(width, height)));
	 //            else if(this.pos.x > width - margin)
	 //                b |= this.collidesAtPos(entity, Vector.add(this.pos, new Vector(-width, height)));
	 //        }
	 //        else if(this.pos.y > height - margin) {
	 //            b |= this.collidesAtPos(entity, Vector.add(this.pos, new Vector(0, -height)));
		// 		if(b) return b;
	 //            if(this.pos.x < margin)
	 //                b |= this.collidesAtPos(entity, Vector.add(this.pos, new Vector(width, -height)));
	 //            else if(this.pos.x > width - margin)
	 //                b |= this.collidesAtPos(entity, Vector.add(this.pos, new Vector(-width, -height)));
	 //        }
		// 	if(b) return b;
	 //        if(this.pos.x < margin) 
	 //            b |= this.collidesAtPos(entity, Vector.add(this.pos, new Vector(width, 0)));
	 //        else if(this.pos.x > width - margin)
	 //            b |= this.collidesAtPos(entity, Vector.add(this.pos, new Vector(-width, 0)));
		// 	if(b) return b;
		// }
	}

	collidesAtPos(entity, pos = this.pos) {
		return Vector.distanceSq(pos, entity.pos) < (this.radius + entity.radius) * (this.radius + entity.radius);
	}
	// collides(entity) {
	// 	return Vector.distanceSq(this.pos, entity.pos) < (this.radius + entity.radius) * (this.radius + entity.radius);
	// }

	collidesWithWalls() {
		if(Main.infiniteMode)
			return false;
		return this.pos.x - this.drawRadius < 0 || this.pos.y - this.drawRadius < 0 ||
			   this.pos.x + this.drawRadius > Main.width || this.pos.y + this.drawRadius > Main.height;
	}

	die() {
		this.inactive = true;
	}

	static preDraw(context) {}
	shouldDraw(drawX, drawY) {
		if(drawX === undefined) {
			drawX = this.getDrawPos().x - Main.screenPos.x;
			drawY = this.getDrawPos().y - Main.screenPos.y;
		}
		return this.drawRadius == -1 || (drawX + this.drawRadius > 0 && drawY + this.drawRadius > 0 &&
			   drawX - this.drawRadius < Main.screenWidth && drawY - this.drawRadius < Main.screenHeight);
	}
	draw(context, drawX, drawY) {
		if(drawX === undefined) {
			drawX = this.getDrawPos().x - Main.screenPos.x;
			drawY = this.getDrawPos().y - Main.screenPos.y;
		}
		if(this.shouldDraw(drawX, drawY))
			this._draw(context, drawX, drawY);

		// if(this.insideLevel && this.levelWrap && Main.currentLevel != null) {
		// 	if(this.shouldDraw(drawX + Main.currentLevel.width, drawY)) {
		// 		this._draw(context, drawX + Main.currentLevel.width, drawY);

		// 		if(this.shouldDraw(drawX + Main.currentLevel.width, drawY + Main.currentLevel.height))
		// 			this._draw(context, drawX + Main.currentLevel.width, drawY + Main.currentLevel.height);
		// 		else if(this.shouldDraw(drawX + Main.currentLevel.width, drawY - Main.currentLevel.height))
		// 			this._draw(context, drawX + Main.currentLevel.width, drawY - Main.currentLevel.height);
		// 	} else if(this.shouldDraw(drawX - Main.currentLevel.width, drawY)) {
		// 		this._draw(context, drawX - Main.currentLevel.width, drawY);

		// 		if(this.shouldDraw(drawX - Main.currentLevel.width, drawY + Main.currentLevel.height))
		// 			this._draw(context, drawX - Main.currentLevel.width, drawY + Main.currentLevel.height);
		// 		else if(this.shouldDraw(drawX - Main.currentLevel.width, drawY - Main.currentLevel.height))
		// 			this._draw(context, drawX - Main.currentLevel.width, drawY -Main.currentLevel.height);
		// 	}

		// 	if(this.shouldDraw(drawX, drawY + Main.currentLevel.height))
		// 		this._draw(context, drawX, drawY + Main.currentLevel.height);
		// 	else if(this.shouldDraw(drawX, drawY - Main.currentLevel.height))
		// 		this._draw(context, drawX, drawY - Main.currentLevel.height);
		// }
	}
	_draw(context, drawX, drawY) {}
	static postDraw(context) {}
}

export class EntityLiving extends Entity {
	constructor(pos, velocity, health) {
		super(pos, velocity)
		this.health = this.maxHealth = health;
		this.attacker = null;
		this.hurtTimer = 0;
	}

	takeDamage(damage, attacker) {
		if(this.hurtTimer > 0)
			return false;
		this.health -= damage;
		this.lastAttacker = attacker;
		this.hurtTimer = 20;
		this.checkHealth();
		return true;
	}

	update() {
		super.update();
		this.checkHealth();
		if(this.hurtTimer > 0)
			this.hurtTimer--;
	}

	checkHealth() {
		if(this.health <= 0)
			this.dieNextTick = true;
	}

	getDrawColor(color, hurtColor = "#fff") {
		if(this.hurtTimer > 0)
			return hurtColor;
		else
			return color;
	}
}

export const EntityManager = {
	entityMap: new Map(),
	register(constructor) {
		this.entityMap.set(constructor, []);
	},
	update() {
		for(var type of this.entityMap.keys()) {
			this.updateArray(this.entityMap.get(type));
		}
	},
	updateArray(entities) {
		for(var entity of entities) {
			if(!entity.inactive) {
				entity.update();
			}
		}
	},
	add(type, entity) {
		this.entityMap.get(type).push(entity);
	},
	getAll(...types) {
		let entities = [];
		for(const type of types) {
			Array.prototype.push.apply(entities, this.entityMap.get(type));
		}
		return entities;
	},
	makeIterator: function*(classes = null) {
		classes = classes || this.entityMap.keys();
		for(const key of classes) {
			let array = this.entityMap.get(key);
			for(const entity of array) {
				yield entity;
			}
		}
	},
	clean() {
		for(var type of this.entityMap.keys()) {
			this.cleanArray(this.entityMap.get(type));
		}
	},
	cleanArray(entities) {
		for(let i = 0; i < entities.length;) {
			if(entities[i].inactive)
				entities.splice(i, 1);
			else 
				i++;
		}
	},
	draw(context) {
		for(var type of this.entityMap.keys()) {
			this.drawArray(type, this.entityMap.get(type), context);
		}
	},
	drawArray(type, entities, context) {
		if(entities.length == 0)
			return;
		type.preDraw(context);
		for(var entity of entities) {
			entity.draw(context);
		}
		type.postDraw(context);
	},
	reset() {
		for(var type of this.entityMap.keys()) {
			this.entityMap.get(type).length = 0;
		}
	}
};

export const EntityUtils = {
	distance(e1, e2) {
		return Math.max(Vector.distance(e1.pos, e2.pos) - e1.radius - e2.radius, 0);
	}
};
