import { Entity, EntityManager, EntityLiving } from "./entity.js";
import { Dust } from "./dust.js";
import { Asteroid } from "./asteroid.js";
import { Vector } from "../vector.js";
import { Player } from "./player.js";
import { Police } from "./police.js";
import { Main, Options } from "../main.js";
import { Utils } from "../utils.js";
import { ColorData } from "../game/data.js";
import { SoundManager, Sounds } from "../game/sound.js";
import { SpriteManager, SpriteIDs } from "../game/sprite.js";

export class Bullet extends Entity {
	//type 0: standard
	//type 1: explosive
	//type 2: speedy penetrator
	//type 3: hostile police
	//type 4: rainbow
	constructor(pos = new Vector(), velocity = new Vector(), type = 0) {
		super(pos, velocity);
		this.levelWrap = false;
		this.color = ColorData.player;
		this.hostile = false;
		this.explosive = false;
		switch(type) {
			case 0:
				this.maxTimeLeft = 50;
				this.radius = this.drawRadius = 2.5;
				break;
			case 1:
				this.explosive = true;
				this.maxTimeLeft = 50;
				this.radius = this.drawRadius = 3.5;
				break;
			case 2:
				this.maxTimeLeft = 80;
				this.radius = this.drawRadius = 3.5;
				break;
			case 3:
				this.maxTimeLeft = 100;
				this.hostile = true;
				this.radius = 4;
				this.radius = this.drawRadius = 3.5;
				this.color = ColorData.police;
				break;
			case 4:
				this.radius = this.drawRadius = 2.5;
				this.rainbow = true;
				Main.totalRainbowBullets++;
				this.rainbowHueOrigin = Main.totalRainbowBullets * 5;
				this.maxTimeLeft = 70;
				break;
		}
		this.type = type;
		this.killed = [];
		this.wallClip = 1;
	}

	update() {
		super.update();
		if(this.rainbow)
			this.color = `hsl(${Main.getRainbowHue(this.rainbowHueOrigin)}, 80%, 60%)`;
		// loop1: for(var i = 0; i < Main.asteroids.length; i++) {
		// 	if(Main.asteroids[i].inactive)
		// 		continue;
		// 	if(this.collides(Main.asteroids[i])) {
		// 		if(this.type == 1) {
		// 			for(var i2 = 0; i2 < Main.asteroids.length; i2++) {
		// 				if(Vector.distanceSq(Main.asteroids[i2].pos, this.pos) < 10000) {
		// 					Main.asteroids[i2].die(this);
		// 					this.killed.push(Main.asteroids[i]);
		// 					this.die();
		// 				}
		// 			}
		// 			break loop1;
		// 		} else {
		// 			if(this.killed.indexOf(Main.asteroids[i].parent) == -1)
		// 				Main.asteroids[i].die(this);
		// 			this.killed.push(Main.asteroids[i]);
		// 			if(this.type != 2)
		// 				this.die();
		// 		}
		// 	}
		// }

		if(!this.inactive) {
			let entities = EntityManager.makeIterator([Asteroid, Player, Police]);
			loop1: for(const entity of entities) {
				if(!entity.inactive && entity.collides(this)) {
					if(!this.shouldDamage(entity))
						continue;

					if(this.type == 1) {
						let entities2 = EntityManager.makeIterator();
						for(const entity2 of entities2) {
							if(!entity.inactive && this.shouldDamage(entity) && Vector.distanceSq(entity2.pos, this.pos) < 10000) {
								entity2.takeDamage(2, this);
								this.killed.push(entity2);
								this.die();
							}
						}
						break loop1;
					} else {
						if(!(entity instanceof Asteroid) || this.killed.indexOf(entity.parent) == -1)
							entity.takeDamage(1, this);
						this.killed.push(entity);
						if(this.type != 2)
							this.die();
					}
				}
			}
		
			if(this.timeLeft == 1) {
				let amount = Math.random(1, 3);
				if(this.explosive) {
					amount += 10;
					for(var i = 0; i < amount; i++) {
						Main.dusts.push(new Dust(Vector.subtract(this.pos, this.velocity), this.velocity.copy().add(Utils.randomVectorSquare(20)), 0.8, this.color));
					}
				} else {
					for(var i = 0; i < amount; i++) {
						Main.dusts.push(new Dust(Vector.subtract(this.pos, this.velocity), Utils.randomVectorSquare(5).add(Vector.divide(this.velocity, 2)), 1, this.color));
					}
				}
			}
		}
	}

	shouldDamage(entity) {
		if(entity == this)
			return;
		return (this.hostile && entity instanceof Player) || (!this.hostile && entity instanceof EntityLiving && !(entity instanceof Player));
	}

	_draw(context, drawX, drawY) {
		let style = this.rainbow ? `hsl(${Main.getRainbowHue(this.rainbowHueOrigin)}, 80%, 60%)` : this.color;
		// context.fillStyle = this.rainbow ? `hsl(${Main.getRainbowHue(this.rainbowHueOrigin)}, 80%, 60%)` : this.color;
		// context.beginPath();
		// context.moveTo(drawX + this.radius, drawY);
		// context.arc(drawX, drawY, this.radius, 0, Math.PI * 2);
		// if(Options.enableShadows) {
		// 	context.shadowBlur = Main.glowBlur / 2;
		// 	context.shadowColor = context.fillStyle;
		// }
		// context.fill();
		// context.shadowBlur = 0;

		let spriteID = Options.enableShadows ? SpriteIDs.bulletShadow : SpriteIDs.bulletNoShadow;
		// alert(spriteID);
		SpriteManager.sprites[spriteID].drawTinted(context, drawX, drawY, style, 0.2 * this.radius);
		// SpriteManager.sprites[spriteID].draw(context, drawX, drawY, 0.2 * this.radius);
	}
}
