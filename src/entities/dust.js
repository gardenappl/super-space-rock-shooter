import { Entity } from "./entity.js";
import { Main, Options } from "../main.js";
import { Vector } from "../vector.js";

export class Dust extends Entity {
	constructor(pos = new Vector(), velocity = new Vector(), timeLeftMultiplier = 1, color = "#fff") {
		// alert(velocity);
		super(pos, velocity);
		this.drawRadius = 3;
		this.radius = 3;
		this.maxTimeLeft = (20 + Math.random() * 15) * timeLeftMultiplier;
		this.color = color;
	}

	update() {
		if(Options.dusts == 0) {
			this.inactive = true;
		}
		super.update();
		// alert("hi");
	}

	shouldDraw() {
		return Options.dusts != 0 && super.shouldDraw();
	}

	_draw(context, drawX = this.pos.x - Main.screenPos.x, drawY = this.pos.y - Main.screenPos.y) {
		context.fillStyle = this.color;
		if(this.timeLeft >= 0 && this.timeLeft < 10)
			context.globalAlpha = this.timeLeft / 10;

		if(Options.dusts == 2) {
			context.shadowBlur = Main.glowBlur;
			context.shadowColor = context.fillStyle;
		}
		context.fillRect(drawX - this.radius / 2, drawY - this.radius / 2, this.radius, this.radius);
		context.shadowBlur = 0;
		context.globalAlpha = 1;
	}
}
