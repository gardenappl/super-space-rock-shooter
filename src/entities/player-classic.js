import { Player } from "./player.js";
import { Main, Options, Controls, ControlType, ScaleType } from "../main.js";
import { Vector } from "../vector.js";
import { Utils } from "../utils.js";
import { Dust } from "./dust.js";
import { Bullet } from "./bullet.js";
import { SoundManager, Sounds } from "../game/sound.js";
import { Entity, EntityManager, EntityLiving } from "./entity.js";
import { Data, ColorData } from "../game/data.js";

export class PlayerClassic extends Player {
	constructor(pos = new Vector(), velocity = new Vector()) {
		super(pos, velocity);
		this.rainbowShootSpread = 1;
	}

	playerMovement() {
		this.shooting = Controls.keyThrust || (Options.control == ControlType.mouse && Controls.mousePressed);
		this.accelerating = Controls.keyShoot || Controls.rightMousePressed;
		if(this.dead != -1) {
			this.accelerating = false;
			this.shooting = false;
		}
		if(Controls.keyPowerup || Controls.middleMousePressed)
			this.usePowerup();
		// console.log(`powerup Main.time: ${this.powerupTime}, powerup type: ${this.powerupType}`);

		if(this.shooting && this.shootDelay == 0) {
			this.shoot(this.rotation, Vector.fromAngle(this.rotation, 10));
		}

		let acceleration = 0;
		// let boosting = false;
		const maxAcceleration = 0.12; //minimal acceleration is 120 / 1500 = 2 / 25

		let mouseVector = Vector.subtract(Vector.add(Controls.mousePos, Main.screenPos), this.pos).multiply(Main.scale);
		this.rotation = Utils.smoothAngleStep(this.rotation, mouseVector.angle, 0.5, 1);
		// this.rotation = mouseVector.angle;
		
		// if(mouseVector.length > 60) {
		// 	acceleration = Utils.clamp((mouseVector.length + 60) / 1500, 0, maxAcceleration);
		// } else {
		// 	acceleration = 0;
		// }
		if(Controls.keyShoot || Controls.rightMousePressed) {
			// boosting = true;

			acceleration = maxAcceleration;

			// this.usePowerup();
		} else {
			acceleration = 0;
		}

		this.velocity.add(Vector.fromAngle(this.rotation, acceleration));
		if(acceleration > 0 && this.dustDelay == 1 && Utils.random(0, 0.1) < acceleration + 0.02) {
			let shootPos = Vector.add(this.pos, Vector.fromAngle(this.rotation, -7).add(Vector.fromAngle(this.rotation + Math.PI / 2, Utils.random(-5, 5))));
			let shootVel = Vector.fromAngle(this.rotation, -5 * acceleration / maxAcceleration).add(Utils.randomVectorSquare(2.5));
			shootVel.add(this.velocity);
			EntityManager.add(Dust, new Dust(shootPos, shootVel, 0.5));
		}


		this.velocity.multiply(0.99);
	}


	_draw(context, drawX = this.pos.x - Main.screenPos.x, drawY = this.pos.y - Main.screenPos.y) {
		// return false;
		context.lineWidth = 2;
    	if(this.currentPowerup == null)
			context.strokeStyle = this.health > 1 ? ColorData.player : "#aaa", "hsl(0, 40%, 90%)";
		else {
			let luma = 50;
			if(this.powerupTime < 300) {
				luma = 75 + Math.sin((50 - this.powerupTime) / 10) * 25;
				luma = luma.toFixed(3);
			}
			let hue = Main.powerupColors[this.powerupType];
			if(this.currentPowerup.id == 4)
				hue = Main.getRainbowHue();
			context.strokeStyle = `hsl(${hue}, 80%, ${luma}%)`;
		}
    	context.beginPath();
    	context.save();
  		context.translate(drawX, drawY); // pivot point
    	context.rotate(this.rotation);

		context.moveTo(11, 0);
		context.lineTo(-9, -7);
		context.lineTo(-6, 0);
		context.lineTo(-9, 7);
		context.closePath();

		if(this.boosting) {
			context.restore();
			context.stroke();
	    	context.save();
	  		context.translate(drawX, drawY); // pivot point
	    	context.rotate(this.rotation);
	    	context.beginPath();
			context.moveTo(0, -15);
			context.arc(0, 0, 15, -0.5 * Math.PI, 0.5 * Math.PI);
		}

    	context.restore();

		if(Options.enableShadows) {
			context.shadowBlur = Main.glowBlur;
			context.shadowColor = context.strokeStyle;
		}
    	context.stroke();
    	context.shadowBlur = 0;

    	// if(Options.control == ControlType.mouse) {
    	// 	Main.setScaling(ScaleType.none);
    	// 	context.lineWidth = 1;
	    // 	context.strokeStyle = "#fff";

	    // 	context.beginPath();
	    // 	context.arc(Main.screenWidth / 2, Main.screenHeight / 2, 60, 0, Math.PI * 2);
	    // 	context.setLineDash([5, 20]);
	    // 	context.stroke();

	    // 	context.beginPath();
	    // 	context.arc(Main.screenWidth / 2, Main.screenHeight / 2, 0.14 * 1500 - 60, 0, Math.PI * 2);
	    // 	context.stroke();

	    // 	context.setLineDash([0]);
    	// 	Main.setScaling(ScaleType.gameplay);
	    // }
	}
}
