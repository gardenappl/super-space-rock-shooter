import { EntityLiving, EntityManager } from "./entity.js";
import { Options } from "../main.js";
import { Vector } from "../vector.js";
import { Pickup } from "./pickup.js";
import { Player } from "./player.js";
import { Dust } from "./dust.js";
import { Utils } from "../utils.js";
import { Bullet } from "./bullet.js";
import { Main } from "../main.js";
import { ColorData } from "../game/data.js";
import { SoundManager, Sounds } from "../game/sound.js";

export class Police extends EntityLiving {
	constructor(pos = new Vector(), velocity = new Vector()) {
		super(pos, velocity, 2);
		this.targetRot = null;
		this.target = null;
		this.shootDelay = 50;
		this.radius = 15;
		this.drawRadius = 30;
		this.initialTimer = 800;
	}

	takeDamage(damage, attacker) {
		super.takeDamage(damage, attacker);
		SoundManager.play(Sounds.hit, this.pos.copy());
	}

	die(killer) {
		super.die(killer);
		SoundManager.play(Sounds.deathEnemy, this.pos.copy());
		EntityManager.add(Pickup, new Pickup(this.pos.copy(), Utils.randomVectorCircle(5), 1, Main.currentLevel.material));
		let amount = Utils.random(10, 15);
		for(var j = 0; j < amount; j++) {
			let dustPos = this.pos.copy().add(Utils.randomVectorCircle(this.radius));
			let dustVel = Utils.randomVectorCircle(5);
			if(killer !== undefined)
				dustVel.add(Vector.divide(this.velocity, 4));
			Main.dusts.push(new Dust(dustPos, dustVel, 1, ColorData.police));
		}
	}

	ai() {
		const shootVelocity = 8;
		const shootRate = 90;
		const radius = 240;
		const moveSpeed = 6;
		const rotSpeed = 0.015;


		if(this.target == null) {
			this.target = Player.findNearest();
		}
		if(this.target.dead != -1) {
			this.velocity.multiply(0.9);
		} else if(this.targetRot == null) {
			let point = this.target.pos.copy();
			let subtracted = Vector.subtract(point, this.pos);
			let newVelocity = subtracted.copy();
			if(newVelocity.length > moveSpeed * 1.5)
				newVelocity.length = moveSpeed * 1.5;
			if(subtracted.length < radius * 1.5) {
				// alert("hi");
				this.initialTimer = 60;
				this.targetRot = subtracted.angle;
			}

			this.velocity.x = Utils.smoothStep(this.velocity.x, newVelocity.x, 0.2);
			this.velocity.y = Utils.smoothStep(this.velocity.y, newVelocity.y, 0.2);
		} else {
			if(this.initialTimer > 0)
				this.initialTimer--;

			this.targetRot += rotSpeed;
			if(this.targetRot > Math.PI * 2)
				this.targetRot = 0;


			let point = Utils.rotate(new Vector(0, radius).add(this.target.pos), this.targetRot, this.target.pos);
			let subtracted = Vector.subtract(point, this.pos);
			let newVelocity = subtracted.copy();
			if(newVelocity.length > moveSpeed)
				newVelocity.length = moveSpeed;
			if(subtracted.length > 700) {
				this.timeLeft = 50;
			}
			// this.velocity = newVelocity;
			// this.radius = Utils.smoothStep(this.radius, 200, 0.001);
			this.velocity.x = Utils.smoothStep(this.velocity.x, newVelocity.x, this.initialTimer > 0 ? 0.35 : 0.15);
			// if(this.velocity.x == undefined)
			// 	alert("asfsaf");
			// // alert(this.velocity.x);
			this.velocity.y = Utils.smoothStep(this.velocity.y, newVelocity.y, this.initialTimer > 0 ? 0.35 : 0.15);

			if(this.initialTimer == 0) {
				this.shootDelay--;
				if(this.shootDelay <= 0) {
					let shootVel = Vector.subtract(this.target.pos, this.pos.copy());
					shootVel.length = shootVelocity;
					// shootVel.add(this.velocity);
					let bullet = new Bullet(this.pos.copy(), shootVel, 3);
					this.shootDelay = shootRate;
					EntityManager.add(Bullet, bullet);
				}
			}
		}
			// this.velocity = new Vector(0.1, 0.1);
		this.rotation = this.velocity.x / 20;
			// console.log(this.velocity);
	}

	static preDraw(context) {
	}

	_draw(context, drawX = this.pos.x - Main.screenPos.x, drawY = this.pos.y - Main.screenPos.y) {
		// return;
		context.strokeStyle = this.getDrawColor(ColorData.police);
		context.lineWidth = 2;


    	context.beginPath();
    	context.save();
  		context.translate(drawX, drawY); // pivot point
    	context.rotate(this.rotation);

		context.arc(0, 0, this.radius, 0, Math.PI);
		context.lineTo(this.radius, 0);
		context.moveTo(-4, 0);
		context.lineTo(-2, -2);
		context.lineTo(2, -2);
		context.lineTo(4, 0);

    	context.restore();
		if(Options.enableShadows) {
			context.shadowBlur = Main.glowBlur;
			context.shadowColor = context.strokeStyle;
		}
    	context.stroke();
    	context.shadowBlur = 0;
	}

	static postDraw(context) {
		// context.stroke();
	}
}