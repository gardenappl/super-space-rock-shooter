import { Entity, EntityManager } from "./entity.js";
import { Vector } from "../vector.js";
import { TextPopup } from "./text-popup.js";
import { Main, Options } from "../main.js";
import { Levels } from "../game/career.js";
import { ColorData, Data } from "../game/data.js";

//type 0: standard asteroid money
//type 1: big police money
//type 2: slowmo powerup
//type 3: super-mode powerup

export class Pickup extends Entity {
	constructor(pos = new Vector(), velocity = new Vector(), type = 0, material = 0) {
		super(pos, velocity);
		this.radius = type == 0 ? 5 : 10;
		this.drawRadius = this.radius + 5;
		this.maxTimeLeft = 750;
		this.type = type;
		this.material = material;
		this.powerupType = 0;
		this.value = 0;
		switch(this.type) {
			case 0:
				this.hue = Main.materialColors[this.material];
				this.value = 1;
				break;
			case 1:
				this.hue = Main.materialColors[this.material];
				this.value = 5;
				break;
			case 2:
				this.powerupType = 3;
				this.hue = ColorData.powerups[3];
				break;
			case 3:
				this.powerupType = 4;
				break;
		}
		this.wallClip = 2;
	}

	update() {
		super.update();
		this.velocity.multiply(0.9);
		if(Main.player.collides(this)) {
			if(this.type <= 1) {
				// if(Main.player.cargo < Career.maxCargo)
				Main.player.cargo += this.value;
				EntityManager.add(TextPopup, new TextPopup(Main.player.pos.copy(), this.value * Data.materialValues[Main.currentLevel.material], ColorData.asteroids[0], Main.defaultFont));
			} else {
				Main.player.availablePowerup = this.powerupType;
				// if(Main.player.powerupType == this.type) {
				// 	Main.player.powerupTime += 300;
				// } else {
				// 	Main.player.powerupTime = 800;
				// }
				// Main.player.powerupType = this.type;
			}
			this.die();
		}
	}

	static preDraw(context) {
		// context.globalAlpha = Main.pickupLuma / 100;
	}

	_draw(context, drawX = this.pos.x - Main.screenPos.x, drawY = this.pos.y - Main.screenPos.y) {
		context.lineWidth = this.type > 0 ? 3 : 2;
		if(this.type == 3)
			context.strokeStyle = `hsl(${Main.getRainbowHue()}, 80%, 60%)`;
		else
			context.strokeStyle = `hsl(${this.hue}, ${Main.pickupLuma}%, ${Main.pickupLuma}%)`;
		let display = true;
		if(this.timeLeft < 150)
			display = Main.time % 10 < 5;
		else if(this.timeLeft < 300)
			display = Main.time % 20 < 10;

		if(display) {
			context.beginPath();
			context.moveTo(drawX + this.radius, drawY);
			context.arc(drawX, drawY, this.radius, 0, Math.PI * 2);
			if(Options.enableShadows) {
				context.shadowBlur = Main.glowBlur;
				context.shadowColor = context.strokeStyle;
			}
			context.stroke();
			context.shadowBlur = 0;
		}
	}

	static postDraw(context) {
		// context.globalAlpha = Main.pickupLuma;
	}
}