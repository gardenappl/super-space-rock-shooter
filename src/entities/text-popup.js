import { Entity } from "./entity.js";
import { Vector } from "../vector.js";
import { Main, Options } from "../main.js";
import { Lang } from "../lang/lang.js";

export class TextPopup extends Entity {
	constructor(pos, text, color, font = Main.largeFont) {
		super(pos, new Vector(0, -4));
		this.drawRadius = -1;
		this.maxTimeLeft = 90;
		this.wallClip = 2;
		this.font = font;
		this.text = text;
		this.color = color;
		this.levelWrap = false;
	}

	update() {
		super.update();
		this.velocity.multiply(0.9);
	}

	_draw(context, drawX = this.pos.x - Main.screenPos.x, drawY = this.pos.y - Main.screenPos.y) {
		if(this.timeLeft < 50) {
			if(this.timeLeft % 10 < 5)
				return;
		}
		context.font = this.font;
		context.textBaseline = "middle";
		context.textAlign = "center";
		context.fillStyle = this.color;
		context.fillText(this.text, drawX, drawY);
	}
}