import { EntityLiving, EntityManager, EntityUtils } from "./entity.js";
import { Dust } from "./dust.js";
import { Pickup } from "./pickup.js";
import { Player } from "./player.js";
import { TextPopup } from "./text-popup.js";
import { Main, Options } from "../main.js";
import { Vector } from "../vector.js";
import { Utils } from "../utils.js";
import { ColorData } from "../game/data.js";
import { Lang } from "../lang/lang.js";
import { SoundManager, Sounds } from "../game/sound.js";

export class Asteroid extends EntityLiving {
	constructor(pos, velocity, type, material, radius = 30 + Math.random() * 20) {
		super(pos, velocity, 1);
		this.radius = radius;
		this.drawRadius = this.radius + 5;
		this.type = type;
		this.material = material;
		// this.wallClip = 1;
		this.angularVelocity = this.radius > 20 ? Utils.random(-0.02, 0.02) : Utils.random(-0.05, 0.05);
		this.points = [];
		let pointCount = Math.floor(3 + Math.PI * this.radius / 6 + Math.random(3));
		let prevAngle = -100;
		let radii = [];
		for(var i = 0; i < pointCount; i++) {
			let angle = Math.PI * 2 / pointCount * i - 0.2 + Math.random() * 0.4;
			if(angle > prevAngle) {
				let radius = this.radius + Math.random() * 10 - 5;
				prevAngle = angle;
				radii.push(radius);
				this.points.push(Vector.fromAngle(angle, radius));
			}
		}
		let radiiTotal = 0;
		for(var i = 0; i < radii.length; i++) {
			radiiTotal += radii[i];
		}
		// this.radius = radiiTotal / radii.length; //average
	}

	ai() {
		// super.update();

		if(Vector.distanceSq(this.pos, Main.player.pos) > Main.screenWidth * Main.screenHeight * 2) {
			this.inactive = true;
		}

		this.rotation += this.angularVelocity;
		// for(var i = 0; i < Main.bullets.length; i++) {
		// 	if(Main.bullets[i].collides(this) && !Main.bullets.inactive) {
		// 		this.die(Main.bullets[i]);
		// 		Main.bullets[i].inactive = true;
		// 		break;
		// 	}
		// }
		// for(var i = 0; i < Main.asteroids.length; i++) {
		// 	if(Main.asteroids[i] != this && Main.asteroids[i].collides(this) && !Main.asteroids.inactive) {
		// 		this.die(Main.asteroids[i]);
		// 		Main.asteroids[i].die(this);
		// 		break;
		// 	}
		// }
	}

	die(killer) {
		SoundManager.play(Sounds.hit, this.pos.copy());
		killer = killer || this.lastAttacker;
		super.die();
		if(this.type == 1) {
			let entities = EntityManager.makeIterator();
			for(const entity of entities) {
				if(entity instanceof EntityLiving && !(entity instanceof Player) && EntityUtils.distance(this, entity) < 300) {
					if(!(entity instanceof Asteroid && entity.type == 3) && entity != this) {
						entity.takeDamage(3, this);
					}
				}
			}
		}
		if(this.type == 3) { //slowmo
			EntityManager.add(Pickup, new Pickup(this.pos, Utils.randomVectorSquare(4), 3, this.material));
			// Main.slowmoTimer = 200;
			// EntityManager.add(TextPopup, new TextPopup(this.pos.copy(), Lang.get("gameplay.slowmo_popup"), ColorData.asteroids[this.type]));
		}
		if(this.radius < 30 && this.type == 0) {
			Main.pickups.push(new Pickup(Vector.add(this.pos, Utils.randomVectorSquare(3)), Utils.randomVectorSquare(2), this.type, this.material));
			// if(Math.random() > 0.05 || Main.player.powerupType > 0) {
			// 	let amount = Math.ceil(Math.random() * 2);
			// 	for(var i = 0; i < amount; i++)
			// 		Main.pickups.push(new Pickup(this.pos.copy(), Utils.randomVectorSquare(3), this.type, this.material));
			// } else {
			// 	let type = Utils.randomInt(1, 3);
			// 	Main.pickups.push(new Pickup(this.pos.copy(), Utils.randomVectorSquare(1), this.type, this.material));
			// }
		}

		if(this.radius > 30 && this.type == 0) {
			let amount = Math.ceil(2, Math.random() * 4);
			for(var i = 0; i < amount; i++) {
				let asteroid = new Asteroid(Utils.randomVectorCircle(0, this.radius).add(this.pos), Vector.add(this.velocity, Utils.randomVectorCircle(1.0, 3.0)), this.type, this.material, Math.random() * 20 + 10);
				asteroid.parent = this;
				Main.asteroids.push(asteroid);
			}
		}
		let amount = Math.floor(10 + Math.random() * (this.radius > 20 ? 10 : 5));
		if(this.type == 1)
			amount *= 5;
		for(var j = 0; j < amount; j++) {
			let dustPos = this.pos.copy().add(Utils.randomVectorCircle(this.radius));
			let dustVel = Utils.randomVectorCircle(5);
			if(killer !== undefined)
				dustVel.add(Vector.divide(killer.velocity, 4));
			if(this.type == 1)
				dustVel.multiply(1);
			Main.dusts.push(new Dust(dustPos, dustVel, this.type == 1 ? 5 : 1.5, ColorData.asteroids[this.type]));
		}
	}

	static preDraw(context) {
		context.lineWidth = 2;
	}

	_draw(context, drawX = this.pos.x - Main.screenPos.x, drawY = this.pos.y - Main.screenPos.y) {
		// context.moveTo(drawX + this.radius, drawY);
		// context.arc(drawX, drawY, this.radius, 0, Math.PI * 2);
		context.strokeStyle = ColorData.asteroids[this.type];
		context.beginPath();
		context.save();
		context.translate(drawX, drawY);
		context.rotate(this.rotation);



		context.moveTo(this.points[0].x, this.points[0].y);
		for(var i = 0; i < this.points.length; i++)
			context.lineTo(this.points[i].x, this.points[i].y);
		context.lineTo(this.points[0].x, this.points[0].y);

		context.restore();

		if(Options.enableShadows) {
			context.shadowBlur = Main.glowBlur;
			context.shadowColor = context.strokeStyle;
		}
		context.stroke();
		context.shadowBlur = 0;
	}

	static postDraw(context) {
	}
}
