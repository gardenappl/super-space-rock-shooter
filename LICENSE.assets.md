#### Menu music

Music by Barry van Oudtshoorn ([barryvan](https://www.barryvan.com.au/)).

* [Background #1](https://indiegamemusic.com/viewtrack.php?id=2724)
* [Background #2](https://indiegamemusic.com/viewtrack.php?id=2725)

I have the author's permission to use the songs for Super Space Rock Shooter with a freeware license.

Found on [IndieGameMusic.com](https://indiegamemusic.com/).

#### Gameplay music

Music by [Podington Bear](https://freemusicarchive.org/music/Podington_Bear).

[Melodic Ambient](https://freemusicarchive.org/music/Podington_Bear/Melodic_Ambient):
* Bountiful
* Celadon
* Fluorescense
* Midnight Blue
* Trillium

Licensed under Creative Commons - CC BY-NC.

Found on [Free Music Archive](https://freemusicarchive.org).
