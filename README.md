# Super Space Rock Shooter

## Initial Dev Setup
1. Install **npm** (comes bundled with **Node.js**).
2. Install **npx**: `npm install -g npx`
2. Download/clone the repository, and in the repo's base directory do: `npm install`

## Build
Use `npm run build` in the repo's base directory. The resulting files should appear in `/out/`.

## License & special thanks

Unless stated otherwise, this code is licensed under [GPLv3](LICENSE.md).

The only exception is the Howler.js audio library, distributed in [sound.js](src/game/sound.js), which uses the [MIT license](LICENSE.howler.md).

### Music

See [LICENSE.assets.md](LICENSE.assets.md) for the song list, and licensing info. Some of the included music is copyrighted and NOT licensed under Creative Commons.

### Sound

Sound effects were created by me, using the free online tool [Leshy SFMaker](https://www.leshylabs.com/apps/sfMaker/). The license is CC BY-NC, or even public domain, whichever you prefer, I don't care.
